#ifndef SHMAP_H
#define SHMAP_H
#ifdef __cplusplus
extern "C" {
#endif


/*
 * shmap.h
 *
 * #SHORTDOC hash map handling (key-value storage)
 *
 * #DOC Map functions handle key-value storage, which is implemented as a
 * #DOC hash table (O(n), with O(1) amortized time complexity for
 * #DOC insert/read/delete)
 * #DOC
 * #DOC
 * #DOC Supported key/value modes (enum eSHM_Type):
 * #DOC
 * #DOC
 * #DOC SHM_II32: int32_t key, int32_t value
 * #DOC
 * #DOC SHM_UU32: uint32_t key, uint32_t value
 * #DOC
 * #DOC SHM_II: int64_t key, int64_t value
 * #DOC
 * #DOC SHM_FF: float key, float value
 * #DOC
 * #DOC SHM_DD: double key, double value
 * #DOC
 * #DOC SHM_SI: string key, int64_t value
 * #DOC
 * #DOC SHM_SU: string key, uint64_t value
 * #DOC
 * #DOC SHM_SD: string key, double value
 * #DOC
 * #DOC SHM_IS: int64_t key, string value
 * #DOC
 * #DOC SHM_US: uint64_t key, string value
 * #DOC
 * #DOC SHM_DS: double key, string value
 * #DOC
 * #DOC SHM_IP: int64_t key, pointer value
 * #DOC
 * #DOC SHM_DP: double key, pointer value
 * #DOC
 * #DOC SHM_SS: string key, string value
 * #DOC
 * #DOC SHM_SP: string key, pointer value
 * #DOC
 * #DOC
 * #DOC Callback types for the shm_itp_*() functions:
 * #DOC
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_ii32)(int32_t k, int32_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_uu32)(uint32_t k, uint32_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_ii)(int64_t k, int64_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_ff)(float k, float v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_dd)(double k, double v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_si)(const srt_string* k, int64_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_su)(const srt_string* k, uint64_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_sd)(const srt_string* k, double v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_is)(int64_t k, const srt_string* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_us)(uint64_t k, const srt_string* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_ds)(double k, const srt_string* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_ip)(int64_t k, const void* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_dp)(double k, const void* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_ss)(const srt_string* k, const srt_string* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_sp)(const srt_string* k, const void* v, void *context);
 *
 * Copyright (c) 2015-2020 F. Aragon. All rights reserved.
 * Released under the BSD 3-Clause License (see the doc/LICENSE)
 */

#include "saux/scommon.h"
#include "saux/sstringo.h"

/*
 * Structures and types
 */

enum eSHM_Type0 {
    SHM0_I32,
    SHM0_U32,
    SHM0_I,
    SHM0_U,
    SHM0_F,
    SHM0_D,
    SHM0_S,
    SHM0_II32,
    SHM0_UU32,
    SHM0_II,
    SHM0_FF,
    SHM0_DD,
    SHM0_SI,
    SHM0_SU,
    SHM0_SD,
    SHM0_IS,
    SHM0_US,
    SHM0_DS,
    SHM0_IP,
    SHM0_DP,
    SHM0_SS,
    SHM0_SP,
    SHM0_NumTypes
};

enum eSHM_Type {
    SHM_II32 = SHM0_II32,
    SHM_UU32 = SHM0_UU32,
    SHM_II = SHM0_II,
    SHM_FF = SHM0_FF,
    SHM_DD = SHM0_DD,
    SHM_SI = SHM0_SI,
    SHM_SU = SHM0_SU,
    SHM_SD = SHM0_SD,
    SHM_IS = SHM0_IS,
    SHM_US = SHM0_US,
    SHM_DS = SHM0_DS,
    SHM_IP = SHM0_IP,
    SHM_DP = SHM0_DP,
    SHM_SS = SHM0_SS,
    SHM_SP = SHM0_SP,
};

struct SHMapi {
    int32_t k;
};
struct SHMapu {
    uint32_t k;
};
struct SHMapI {
    int64_t k;
};
struct SHMapU {
    uint64_t k;
};
struct SHMapF {
    float k;
};
struct SHMapD {
    double k;
};

struct SHMapS {
    srt_stringo1 k;
};

struct SHMapSS {
    srt_stringo kv;
};

struct SHMapii {
    struct SHMapi x;
    int32_t v;
};
struct SHMapuu {
    struct SHMapu x;
    uint32_t v;
};
struct SHMapII {
    struct SHMapI x;
    int64_t v;
};
struct SHMapFF {
    struct SHMapF x;
    float v;
};
struct SHMapDD {
    struct SHMapD x;
    double v;
};

struct SHMapIS {
    struct SHMapI x;
    srt_stringo1 v;
};
struct SHMapUS {
    struct SHMapU x;
    srt_stringo1 v;
};
struct SHMapDS {
    struct SHMapD x;
    srt_stringo1 v;
};

struct SHMapIP {
    struct SHMapI x;
    const void *v;
};
struct SHMapDP {
    struct SHMapD x;
    const void *v;
};
struct SHMapSP {
    struct SHMapS x;
    const void *v;
};

struct SHMapSI {
    struct SHMapS x;
    int64_t v;
};
struct SHMapSU {
    struct SHMapS x;
    uint64_t v;
};
struct SHMapSD {
    struct SHMapS x;
    double v;
};

typedef struct S_HMap srt_hmap;

typedef uint32_t shm_eloc_t_; /* element location offset */

struct SHMBucket {
    /*
     * Location where the bucket associated data is stored
     */
    shm_eloc_t_ loc;
    /*
     * Hash of the element (the bucket id would be the N highest bits)
     */
    uint32_t hash;
    /*
     * Bucket collision counter
     * 0: Zero elements associated to the bucket. This means that no
     *    element with the hash associated to the bucket has been inserted
     * >= 1: Number of elements associated to the bucket.
     */
    uint32_t cnt;
};

/*
 * srt_hmap memory layout:
 *
 * | SDataFull | struct fields | struct SHMBucket [N] | elements [M] |
 */

typedef srt_bool (*shm_eq_f)(const void *key, const void *node);
typedef void (*shm_del_f)(void *node);
typedef uint32_t (*shm_hash_f)(const void *node);
typedef const void *(*shm_n2key_f)(const void *node);

struct S_HMap {
    struct SDataFull d;
    uint32_t hbits; /* hash table bits */
    uint32_t hmask; /* hash table bitmask */
    size_t rh_threshold; /* (1 << hbits) * rh_threshold_pct) / 100 */
    size_t rh_threshold_pct;
};

/*
 * Configuration
 */

#define SHM_HASH_32(k)  sh_hash32((uint32_t)(k))
#define SHM_HASH_64(k)  sh_hash64((uint64_t)(k))
#define SHM_HASH_F(k)   sh_hash_f(k)
#define SHM_HASH_D(k)   sh_hash_d(k)

#ifdef S_FORCE_USING_MURMUR3
#define SHM_HASH_S ss_mh3_32
#else
#define SHM_HASH_S ss_fnv1a
#endif

/*
 * Allocation
 */

S_INLINE uint8_t shm_elem_size(int t)
{
    switch (t) {
        case SHM0_I32: return sizeof(struct SHMapi);
        case SHM0_U32: return sizeof(struct SHMapu);
        case SHM0_I: return sizeof(struct SHMapI);
        case SHM0_U: return sizeof(struct SHMapU);
        case SHM0_F: return sizeof(struct SHMapF);
        case SHM0_D: return sizeof(struct SHMapD);
        case SHM0_S: return sizeof(struct SHMapS);
        case SHM0_II32: return sizeof(struct SHMapii);
        case SHM0_UU32: return sizeof(struct SHMapuu);
        case SHM0_II: return sizeof(struct SHMapII);
        case SHM0_FF: return sizeof(struct SHMapFF);
        case SHM0_DD: return sizeof(struct SHMapDD);
        case SHM0_SI: return sizeof(struct SHMapSI);
        case SHM0_SU: return sizeof(struct SHMapSU);
        case SHM0_SD: return sizeof(struct SHMapSD);
        case SHM0_IS: return sizeof(struct SHMapIS);
        case SHM0_US: return sizeof(struct SHMapUS);
        case SHM0_DS: return sizeof(struct SHMapDS);
        case SHM0_IP: return sizeof(struct SHMapIP);
        case SHM0_DP: return sizeof(struct SHMapDP);
        case SHM0_SS: return sizeof(struct SHMapSS);
        case SHM0_SP: return sizeof(struct SHMapSP);
        default: break;
    }
    return 0;
}

S_INLINE size_t sh_hdr0_size(void)
{
    size_t as = sizeof(void *);
    return (sizeof(srt_hmap) / as) * as + ((sizeof(srt_hmap) % as) ? as : 0);
}

S_INLINE size_t sh_hdr_size(int t, uint64_t np2_elems)
{
    size_t h0s = sh_hdr0_size(), hs, es = shm_elem_size(t), hsr;
    uint64_t hs64 = h0s + np2_elems * sizeof(struct SHMBucket);
    hs = (size_t)hs64;
    RETURN_IF((uint64_t)hs != hs64, 0);
    hsr = es ? hs % es : 0;
    return hsr ? hs - hsr + es : hs;
}

S_INLINE  struct SHMBucket *shm_get_buckets( srt_hmap *hm) {
    return ( struct SHMBucket *)(( uint8_t *)hm
                                        + sh_hdr0_size());
}
S_INLINE const struct SHMBucket *shm_get_buckets_r(const srt_hmap *hm) {
    return (const struct SHMBucket *)((const uint8_t *)hm
                                        + sh_hdr0_size());
}

S_INLINE unsigned shm_s2hb(size_t max_size)
{
    unsigned hbits = slog2_ceil(max_size);
    return hbits ? hbits : 1;
}

/*
#API: |Allocate hash map (stack)|hash map type; initial reserve|hmap|O(n)|1;2|
srt_hmap *shm_alloca(enum eSHM_Type t, size_t n);
*/
#define shm_alloca(type, max_size)                             \
    shm_alloc_raw(type, S_TRUE,                                \
             s_alloca(sd_alloc_size_raw(                       \
                sh_hdr_size(type, snextpow2(max_size)),        \
                shm_elem_size(type), max_size, S_FALSE)),      \
             sh_hdr_size(type, (size_t)snextpow2(max_size)),   \
             shm_elem_size(type), max_size, shm_s2hb(max_size))

srt_hmap *shm_alloc_raw(int t, srt_bool ext_buf, void *buffer, size_t hdr_size,
            size_t elem_size, size_t max_size, size_t np2_size);

srt_hmap *shm_alloc_aux(int t, size_t init_size);

/* #api: |allocate hash map (heap)|hash map type; initial reserve|hmap|O(n)|1;2| */
S_INLINE srt_hmap *shm_alloc(enum eSHM_Type t, size_t init_size)
{
    return shm_alloc_aux((int)t, init_size);
}

S_INLINE size_t shm_size(const srt_hmap *c)
{
    return sd_size((const srt_data *)c);
}

S_INLINE void shm_set_size(srt_hmap *c, size_t s)
{
    sd_set_size((srt_data *)c, s);
}

S_INLINE size_t shm_max_size(const srt_hmap *c)
{
    return sd_max_size((const srt_data *)c);
}

S_INLINE size_t shm_alloc_size(const srt_hmap *c)
{
    return sd_alloc_size((const srt_data *)c);
}

S_INLINE size_t shm_get_buffer_size(const srt_hmap *c)
{
    return sd_size((const srt_data *)c)
         * sd_elem_size((const srt_data *)c);
}

S_INLINE void *shm_elem_addr(srt_hmap *c, size_t pos)
{
    return sd_elem_addr((srt_data *)c, pos);
}

S_INLINE const void *shm_elem_addr_r(const srt_hmap *c, size_t pos)
{
    return sd_elem_addr_r((const srt_data *)c, pos);
}

S_INLINE size_t shm_capacity(const srt_hmap *c)
{
    return sd_max_size((const srt_data *)c);
}

S_INLINE size_t shm_capacity_left(const srt_hmap *c)
{
    return sd_max_size((const srt_data *)c)
         - sd_size((const srt_data *)c);
}

S_INLINE size_t shm_len(const srt_hmap *c)
{
    return sd_size((const srt_data *)c);
}
S_INLINE uint8_t *shm_get_buffer(srt_hmap *c)
{
    return sd_get_buffer((srt_data *)c);
}

S_INLINE const uint8_t *shm_get_buffer_r(const srt_hmap *c)
{
    return sd_get_buffer_r((const srt_data *)c);
}
S_INLINE srt_hmap *shm_shrink(srt_hmap **c)
{
    return (srt_hmap *)sd_shrink((srt_data **)c, 0);
}

S_INLINE srt_bool shm_empty(const srt_hmap *c)
{
    return shm_size(c) == 0 ? S_TRUE : S_FALSE;
}

S_INLINE void shm_set_alloc_errors(srt_hmap *c)
{
    sd_set_alloc_errors((srt_data *)c);
}

S_INLINE void shm_reset_alloc_errors(srt_hmap *c)
{
    sd_reset_alloc_errors((srt_data *)c);
}

S_INLINE srt_bool shm_alloc_errors(srt_hmap *c)
{
    return sd_alloc_errors((srt_data *)c);
}

S_INLINE size_t shm_grow(srt_hmap **c, size_t extra_elems)
{
    return sd_grow((srt_data **)c, extra_elems, 0);
}

S_INLINE size_t shm_reserve(srt_hmap **c, size_t max_elems)
{
    return sd_reserve((srt_data **)c, max_elems, 0);
}

/*
#API: |Ensure space for extra elements|hash map;number of extra elements|extra size allocated|O(1)|1;2|
size_t shm_grow(srt_hmap **hm, size_t extra_elems)

#API: |Ensure space for elements|hash map;absolute element reserve|reserved elements|O(1)|1;2|
size_t shm_reserve(srt_hmap **hm, size_t max_elems)

#API: |Make the hmap use the minimum possible memory|hmap|hmap reference (optional usage)|O(1) for allocators using memory remap; O(n) for naive allocators|1;2|
srt_hmap *shm_shrink(srt_hmap **hm);

#API: |Get hmap size|hmap|Hash map number of elements|O(1)|1;2|
size_t shm_size(const srt_hmap *hm);

#API: |Get hmap size|hmap|Hash map current max number of elements|O(1)|1;2|
size_t shm_max_size(const srt_hmap *hm);

#API: |Allocated space|hmap|current allocated space (vector elements)|O(1)|1;2|
size_t shm_capacity(const srt_hmap *hm);

#API: |Preallocated space left|hmap|allocated space left|O(1)|1;2|
size_t shm_capacity_left(const srt_hmap *hm);

#API: |Tells if a hash map is empty (zero elements)|hmap|S_TRUE: empty; S_FALSE: not empty|O(1)|1;2|
srt_bool shm_empty(const srt_hmap *hm)
*/

/* #API: |Duplicate hash map|input map|output map|O(n)|1;2| */
srt_hmap *shm_dup(const srt_hmap *src);

/* #API: |Clear/reset map (keeping map type)|hmap||O(1) for simple maps, O(n) for maps having nodes with strings|0;1| */
void shm_clear(srt_hmap *hm);

/*
#API: |Free one or more hash maps|hash map; more hash maps (optional)|-|O(1) for simple dmaps, O(n) for dmaps having nodes with strings|1;2|
void shm_free(srt_hmap **hm, ...)
*/
#ifdef S_USE_VA_ARGS
#define shm_free(...) shm_free_aux(__VA_ARGS__, S_INVALID_PTR_VARG_TAIL)
#else
#define shm_free(m) shm_free_aux(m, S_INVALID_PTR_VARG_TAIL)
#endif
void shm_free_aux(srt_hmap **s, ...);

/*
 * Copy
 */

/* #API: |Overwrite map with a map copy|output hash map; input map|output map reference (optional usage)|O(n)|0;1| */
srt_hmap *shm_cpy(srt_hmap **hm, const srt_hmap *src);

/*
 * Random access
 */

const void *shm_at(const srt_hmap *hm, uint32_t h, const void *key, uint32_t *tl);

S_INLINE const void *shm_at_s(const srt_hmap *hm, uint32_t h, const void *key, uint32_t *tl)
{
    return hm ? shm_at(hm, h, key, tl) : NULL;
}

/* #API: |Access to element (SHM_II32)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE int32_t shm_at_ii32(const srt_hmap *hm, int32_t k)
{
    const struct SHMapii *e = (const struct SHMapii *)
                shm_at_s(hm, SHM_HASH_32(k),
                         &k, NULL);
    return e ? e->v : 0;
}

/* #API: |Access to element (SHM_II32)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_ii32(const srt_hmap *hm, int32_t k, int32_t *v)
{
    const struct SHMapii *e = (const struct SHMapii *)
                shm_at_s(hm, SHM_HASH_32(k),
                         &k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_UU32)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE uint32_t shm_at_uu32(const srt_hmap *hm, uint32_t k)
{
    const struct SHMapuu *e = (const struct SHMapuu *)
                shm_at_s(hm, SHM_HASH_32(k),
                         &k, NULL);
    return e ? e->v : 0;
}

/* #API: |Access to element (SHM_UU32)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_uu32(const srt_hmap *hm, uint32_t k, uint32_t *v)
{
    const struct SHMapuu *e = (const struct SHMapuu *)
                shm_at_s(hm, SHM_HASH_32(k),
                         &k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_II)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE int64_t shm_at_ii(const srt_hmap *hm, int64_t k)
{
    const struct SHMapII *e = (const struct SHMapII *)
                shm_at_s(hm, SHM_HASH_64(k),
                         &k, NULL);
    return e ? e->v : 0;
}

/* #API: |Access to element (SHM_II)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_ii(const srt_hmap *hm, int64_t k, int64_t *v)
{
    const struct SHMapII *e = (const struct SHMapII *)
                shm_at_s(hm, SHM_HASH_64(k),
                         &k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_FF)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE float shm_at_ff(const srt_hmap *hm, float k)
{
    const struct SHMapFF *e = (const struct SHMapFF *)
                shm_at_s(hm, SHM_HASH_F(k),
                         &k, NULL);
    return e ? e->v : 0;
}

/* #API: |Access to element (SHM_FF)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_ff(const srt_hmap *hm, float k, float *v)
{
    const struct SHMapFF *e = (const struct SHMapFF *)
                shm_at_s(hm, SHM_HASH_F(k),
                         &k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_DD)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE double shm_at_dd(const srt_hmap *hm, double k)
{
    const struct SHMapDD *e = (const struct SHMapDD *)
                shm_at_s(hm, SHM_HASH_D(k),
                         &k, NULL);
    return e ? e->v : 0;
}

/* #API: |Access to element (SHM_DD)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_dd(const srt_hmap *hm, double k, double *v)
{
    const struct SHMapDD *e = (const struct SHMapDD *)
                shm_at_s(hm, SHM_HASH_D(k),
                         &k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_SI)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE int64_t shm_at_si(const srt_hmap *hm, const srt_string* k)
{
    const struct SHMapSI *e = (const struct SHMapSI *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    return e ? e->v : 0;
}

/* #API: |Access to element (SHM_SI)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_si(const srt_hmap *hm, const srt_string* k, int64_t *v)
{
    const struct SHMapSI *e = (const struct SHMapSI *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_SU)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE uint64_t shm_at_su(const srt_hmap *hm, const srt_string* k)
{
    const struct SHMapSU *e = (const struct SHMapSU *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    return e ? e->v : 0;
}

/* #API: |Access to element (SHM_SU)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_su(const srt_hmap *hm, const srt_string* k, uint64_t *v)
{
    const struct SHMapSU *e = (const struct SHMapSU *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_SD)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE double shm_at_sd(const srt_hmap *hm, const srt_string* k)
{
    const struct SHMapSD *e = (const struct SHMapSD *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    return e ? e->v : 0;
}

/* #API: |Access to element (SHM_SD)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_sd(const srt_hmap *hm, const srt_string* k, double *v)
{
    const struct SHMapSD *e = (const struct SHMapSD *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_IS)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE const srt_string *shm_at_is(const srt_hmap *hm, int64_t k)
{
    const struct SHMapIS *e = (const struct SHMapIS *)
                shm_at_s(hm, SHM_HASH_64(k),
                         &k, NULL);
    return e ? sso1_get((const srt_stringo1 *)
                                              &e->v) : ss_void;
}

/* #API: |Access to element (SHM_IS)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_is(const srt_hmap *hm, int64_t k, const srt_string* *v)
{
    const struct SHMapIS *e = (const struct SHMapIS *)
                shm_at_s(hm, SHM_HASH_64(k),
                         &k, NULL);
    if(e && v) {
        *v = sso1_get((const srt_stringo1 *)
                                              &e->v);
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_US)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE const srt_string *shm_at_us(const srt_hmap *hm, uint64_t k)
{
    const struct SHMapUS *e = (const struct SHMapUS *)
                shm_at_s(hm, SHM_HASH_64(k),
                         &k, NULL);
    return e ? sso1_get((const srt_stringo1 *)
                                              &e->v) : ss_void;
}

/* #API: |Access to element (SHM_US)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_us(const srt_hmap *hm, uint64_t k, const srt_string* *v)
{
    const struct SHMapUS *e = (const struct SHMapUS *)
                shm_at_s(hm, SHM_HASH_64(k),
                         &k, NULL);
    if(e && v) {
        *v = sso1_get((const srt_stringo1 *)
                                              &e->v);
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_DS)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE const srt_string *shm_at_ds(const srt_hmap *hm, double k)
{
    const struct SHMapDS *e = (const struct SHMapDS *)
                shm_at_s(hm, SHM_HASH_D(k),
                         &k, NULL);
    return e ? sso1_get((const srt_stringo1 *)
                                              &e->v) : ss_void;
}

/* #API: |Access to element (SHM_DS)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_ds(const srt_hmap *hm, double k, const srt_string* *v)
{
    const struct SHMapDS *e = (const struct SHMapDS *)
                shm_at_s(hm, SHM_HASH_D(k),
                         &k, NULL);
    if(e && v) {
        *v = sso1_get((const srt_stringo1 *)
                                              &e->v);
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_IP)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE const void *shm_at_ip(const srt_hmap *hm, int64_t k)
{
    const struct SHMapIP *e = (const struct SHMapIP *)
                shm_at_s(hm, SHM_HASH_64(k),
                         &k, NULL);
    return e ? e->v : NULL;
}

/* #API: |Access to element (SHM_IP)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_ip(const srt_hmap *hm, int64_t k, const void* *v)
{
    const struct SHMapIP *e = (const struct SHMapIP *)
                shm_at_s(hm, SHM_HASH_64(k),
                         &k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_DP)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE const void *shm_at_dp(const srt_hmap *hm, double k)
{
    const struct SHMapDP *e = (const struct SHMapDP *)
                shm_at_s(hm, SHM_HASH_D(k),
                         &k, NULL);
    return e ? e->v : NULL;
}

/* #API: |Access to element (SHM_DP)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_dp(const srt_hmap *hm, double k, const void* *v)
{
    const struct SHMapDP *e = (const struct SHMapDP *)
                shm_at_s(hm, SHM_HASH_D(k),
                         &k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_SS)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE const srt_string *shm_at_ss(const srt_hmap *hm, const srt_string* k)
{
    const struct SHMapSS *e = (const struct SHMapSS *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    return e ? sso_get_s2(&e->kv) : ss_void;
}

/* #API: |Access to element (SHM_SS)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_ss(const srt_hmap *hm, const srt_string* k, const srt_string* *v)
{
    const struct SHMapSS *e = (const struct SHMapSS *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    if(e && v) {
        *v = sso_get_s2(&e->kv);
        return S_TRUE;
    } else
        return S_FALSE;
}
/* #API: |Access to element (SHM_SP)|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE const void *shm_at_sp(const srt_hmap *hm, const srt_string* k)
{
    const struct SHMapSP *e = (const struct SHMapSP *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    return e ? e->v : NULL;
}

/* #API: |Access to element (SHM_SP)|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_sp(const srt_hmap *hm, const srt_string* k, const void* *v)
{
    const struct SHMapSP *e = (const struct SHMapSP *)
                shm_at_s(hm, SHM_HASH_S(k),
                         k, NULL);
    if(e && v) {
        *v = e->v;
        return S_TRUE;
    } else
        return S_FALSE;
}

/*
 * Existence check
 */

/* #API: |Map element count/check (SHM_I*32)|hash map; key|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE size_t shm_count_i32(const srt_hmap *hm, int32_t k)
{
    return shm_at_s(hm, SHM_HASH_32(k),
                    &k, NULL) ? 1 : 0;
}
/* #API: |Map element count/check (SHM_U*32)|hash map; key|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE size_t shm_count_u32(const srt_hmap *hm, uint32_t k)
{
    return shm_at_s(hm, SHM_HASH_32(k),
                    &k, NULL) ? 1 : 0;
}
/* #API: |Map element count/check (SHM_I*)|hash map; key|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE size_t shm_count_i(const srt_hmap *hm, int64_t k)
{
    return shm_at_s(hm, SHM_HASH_64(k),
                    &k, NULL) ? 1 : 0;
}
/* #API: |Map element count/check (SHM_U*)|hash map; key|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE size_t shm_count_u(const srt_hmap *hm, uint64_t k)
{
    return shm_at_s(hm, SHM_HASH_64(k),
                    &k, NULL) ? 1 : 0;
}
/* #API: |Map element count/check (SHM_F*)|hash map; key|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE size_t shm_count_f(const srt_hmap *hm, float k)
{
    return shm_at_s(hm, SHM_HASH_F(k),
                    &k, NULL) ? 1 : 0;
}
/* #API: |Map element count/check (SHM_D*)|hash map; key|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE size_t shm_count_d(const srt_hmap *hm, double k)
{
    return shm_at_s(hm, SHM_HASH_D(k),
                    &k, NULL) ? 1 : 0;
}
/* #API: |Map element count/check (SHM_S*)|hash map; key|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE size_t shm_count_s(const srt_hmap *hm, const srt_string* k)
{
    return shm_at_s(hm, SHM_HASH_S(k),
                    k, NULL) ? 1 : 0;
}

/*
 * Insert
 */

/* #API: |Insert into map (SHM_II32)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_ii32(srt_hmap **hm, int32_t k, int32_t v);
/* #API: |Insert into map (SHM_UU32)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_uu32(srt_hmap **hm, uint32_t k, uint32_t v);
/* #API: |Insert into map (SHM_II)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_ii(srt_hmap **hm, int64_t k, int64_t v);
/* #API: |Insert into map (SHM_FF)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_ff(srt_hmap **hm, float k, float v);
/* #API: |Insert into map (SHM_DD)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_dd(srt_hmap **hm, double k, double v);
/* #API: |Insert into map (SHM_SI)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_si(srt_hmap **hm, const srt_string* k, int64_t v);
/* #API: |Insert into map (SHM_SU)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_su(srt_hmap **hm, const srt_string* k, uint64_t v);
/* #API: |Insert into map (SHM_SD)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_sd(srt_hmap **hm, const srt_string* k, double v);
/* #API: |Insert into map (SHM_IS)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_is(srt_hmap **hm, int64_t k, const srt_string* v);
/* #API: |Insert into map (SHM_US)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_us(srt_hmap **hm, uint64_t k, const srt_string* v);
/* #API: |Insert into map (SHM_DS)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_ds(srt_hmap **hm, double k, const srt_string* v);
/* #API: |Insert into map (SHM_IP)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_ip(srt_hmap **hm, int64_t k, const void* v);
/* #API: |Insert into map (SHM_DP)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_dp(srt_hmap **hm, double k, const void* v);
/* #API: |Insert into map (SHM_SS)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_ss(srt_hmap **hm, const srt_string* k, const srt_string* v);
/* #API: |Insert into map (SHM_SP)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_sp(srt_hmap **hm, const srt_string* k, const void* v);

/* Hash set support (proxy) */

srt_bool shm_insert_i32(srt_hmap **hm, int32_t k);
srt_bool shm_insert_u32(srt_hmap **hm, uint32_t k);
srt_bool shm_insert_i(srt_hmap **hm, int64_t k);
srt_bool shm_insert_u(srt_hmap **hm, uint64_t k);
srt_bool shm_insert_f(srt_hmap **hm, float k);
srt_bool shm_insert_d(srt_hmap **hm, double k);
srt_bool shm_insert_s(srt_hmap **hm, const srt_string* k);

/*
 * Increment
 */

/* #API: |Increment value map element (SHM_II32)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_ii32(srt_hmap **hm, int32_t k, int32_t v);
/* #API: |Increment value map element (SHM_UU32)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_uu32(srt_hmap **hm, uint32_t k, uint32_t v);
/* #API: |Increment value map element (SHM_II)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_ii(srt_hmap **hm, int64_t k, int64_t v);
/* #API: |Increment value map element (SHM_FF)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_ff(srt_hmap **hm, float k, float v);
/* #API: |Increment value map element (SHM_DD)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_dd(srt_hmap **hm, double k, double v);
/* #API: |Increment value map element (SHM_SI)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_si(srt_hmap **hm, const srt_string* k, int64_t v);
/* #API: |Increment value map element (SHM_SU)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_su(srt_hmap **hm, const srt_string* k, uint64_t v);
/* #API: |Increment value map element (SHM_SD)|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_sd(srt_hmap **hm, const srt_string* k, double v);

/*
 * Delete
 */

/* #API: |Delete map element (SHM_I*32)|hash map; key|S_TRUE: found and deleted; S_FALSE: not found|O(n), O(1) average amortized|1;2| */
srt_bool shm_delete_i32(srt_hmap *hm, int32_t k);
/* #API: |Delete map element (SHM_U*32)|hash map; key|S_TRUE: found and deleted; S_FALSE: not found|O(n), O(1) average amortized|1;2| */
srt_bool shm_delete_u32(srt_hmap *hm, uint32_t k);
/* #API: |Delete map element (SHM_I*)|hash map; key|S_TRUE: found and deleted; S_FALSE: not found|O(n), O(1) average amortized|1;2| */
srt_bool shm_delete_i(srt_hmap *hm, int64_t k);
/* #API: |Delete map element (SHM_U*)|hash map; key|S_TRUE: found and deleted; S_FALSE: not found|O(n), O(1) average amortized|1;2| */
srt_bool shm_delete_u(srt_hmap *hm, uint64_t k);
/* #API: |Delete map element (SHM_F*)|hash map; key|S_TRUE: found and deleted; S_FALSE: not found|O(n), O(1) average amortized|1;2| */
srt_bool shm_delete_f(srt_hmap *hm, float k);
/* #API: |Delete map element (SHM_D*)|hash map; key|S_TRUE: found and deleted; S_FALSE: not found|O(n), O(1) average amortized|1;2| */
srt_bool shm_delete_d(srt_hmap *hm, double k);
/* #API: |Delete map element (SHM_S*)|hash map; key|S_TRUE: found and deleted; S_FALSE: not found|O(n), O(1) average amortized|1;2| */
srt_bool shm_delete_s(srt_hmap *hm, const srt_string* k);

/*
 * Enumeration
 */

S_INLINE const uint8_t *shm_enum_r(const srt_hmap *h, size_t i)
{
    return h && i < shm_size(h) ? shm_get_buffer_r(h) + i * h->d.elem_size : NULL;
}

/* #API: |Enumerate map keys (SHM_I32)|hash map; element, 0 to n - 1|int32_t|O(1)|1;2| */
S_INLINE int32_t shm_it_i32_k(const srt_hmap *hm, size_t i)
{
    const struct SHMapi *n = (const struct SHMapi *)shm_enum_r(hm, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SHM_U32)|hash map; element, 0 to n - 1|uint32_t|O(1)|1;2| */
S_INLINE uint32_t shm_it_u32_k(const srt_hmap *hm, size_t i)
{
    const struct SHMapu *n = (const struct SHMapu *)shm_enum_r(hm, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SHM_I)|hash map; element, 0 to n - 1|int64_t|O(1)|1;2| */
S_INLINE int64_t shm_it_i_k(const srt_hmap *hm, size_t i)
{
    const struct SHMapI *n = (const struct SHMapI *)shm_enum_r(hm, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SHM_U)|hash map; element, 0 to n - 1|uint64_t|O(1)|1;2| */
S_INLINE uint64_t shm_it_u_k(const srt_hmap *hm, size_t i)
{
    const struct SHMapU *n = (const struct SHMapU *)shm_enum_r(hm, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SHM_F)|hash map; element, 0 to n - 1|float|O(1)|1;2| */
S_INLINE float shm_it_f_k(const srt_hmap *hm, size_t i)
{
    const struct SHMapF *n = (const struct SHMapF *)shm_enum_r(hm, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SHM_D)|hash map; element, 0 to n - 1|double|O(1)|1;2| */
S_INLINE double shm_it_d_k(const srt_hmap *hm, size_t i)
{
    const struct SHMapD *n = (const struct SHMapD *)shm_enum_r(hm, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SHM_S)|hash map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *shm_it_s_k(const srt_hmap *hm, size_t i)
{
    const struct SHMapS *n = (const struct SHMapS *)shm_enum_r(hm, i);
    RETURN_IF(!n, ss_void);
    return sso_get((const srt_stringo *)
                                  &n->k);
}

/* #API: |Enumerate map values (SHM_II32)|hash map; element, 0 to n - 1|int32_t|O(1)|1;2| */
S_INLINE int32_t shm_it_ii32_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapii *e;
    RETURN_IF(!hm || SHM_II32 != hm->d.sub_type, 0);
    e = (const struct SHMapii *)shm_enum_r(hm, i);
    RETURN_IF(!e, 0);
    return e->v;
}
/* #API: |Enumerate map values (SHM_UU32)|hash map; element, 0 to n - 1|uint32_t|O(1)|1;2| */
S_INLINE uint32_t shm_it_uu32_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapuu *e;
    RETURN_IF(!hm || SHM_UU32 != hm->d.sub_type, 0);
    e = (const struct SHMapuu *)shm_enum_r(hm, i);
    RETURN_IF(!e, 0);
    return e->v;
}
/* #API: |Enumerate map values (SHM_II)|hash map; element, 0 to n - 1|int64_t|O(1)|1;2| */
S_INLINE int64_t shm_it_ii_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapII *e;
    RETURN_IF(!hm || SHM_II != hm->d.sub_type, 0);
    e = (const struct SHMapII *)shm_enum_r(hm, i);
    RETURN_IF(!e, 0);
    return e->v;
}
/* #API: |Enumerate map values (SHM_FF)|hash map; element, 0 to n - 1|float|O(1)|1;2| */
S_INLINE float shm_it_ff_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapFF *e;
    RETURN_IF(!hm || SHM_FF != hm->d.sub_type, 0);
    e = (const struct SHMapFF *)shm_enum_r(hm, i);
    RETURN_IF(!e, 0);
    return e->v;
}
/* #API: |Enumerate map values (SHM_DD)|hash map; element, 0 to n - 1|double|O(1)|1;2| */
S_INLINE double shm_it_dd_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapDD *e;
    RETURN_IF(!hm || SHM_DD != hm->d.sub_type, 0);
    e = (const struct SHMapDD *)shm_enum_r(hm, i);
    RETURN_IF(!e, 0);
    return e->v;
}
/* #API: |Enumerate map values (SHM_SI)|hash map; element, 0 to n - 1|int64_t|O(1)|1;2| */
S_INLINE int64_t shm_it_si_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapSI *e;
    RETURN_IF(!hm || SHM_SI != hm->d.sub_type, 0);
    e = (const struct SHMapSI *)shm_enum_r(hm, i);
    RETURN_IF(!e, 0);
    return e->v;
}
/* #API: |Enumerate map values (SHM_SU)|hash map; element, 0 to n - 1|uint64_t|O(1)|1;2| */
S_INLINE uint64_t shm_it_su_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapSU *e;
    RETURN_IF(!hm || SHM_SU != hm->d.sub_type, 0);
    e = (const struct SHMapSU *)shm_enum_r(hm, i);
    RETURN_IF(!e, 0);
    return e->v;
}
/* #API: |Enumerate map values (SHM_SD)|hash map; element, 0 to n - 1|double|O(1)|1;2| */
S_INLINE double shm_it_sd_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapSD *e;
    RETURN_IF(!hm || SHM_SD != hm->d.sub_type, 0);
    e = (const struct SHMapSD *)shm_enum_r(hm, i);
    RETURN_IF(!e, 0);
    return e->v;
}
/* #API: |Enumerate map values (SHM_IS)|hash map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *shm_it_is_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapIS *e;
    RETURN_IF(!hm || SHM_IS != hm->d.sub_type, ss_void);
    e = (const struct SHMapIS *)shm_enum_r(hm, i);
    RETURN_IF(!e, ss_void);
    return sso1_get((const srt_stringo1 *)
                                              &e->v);
}
/* #API: |Enumerate map values (SHM_US)|hash map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *shm_it_us_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapUS *e;
    RETURN_IF(!hm || SHM_US != hm->d.sub_type, ss_void);
    e = (const struct SHMapUS *)shm_enum_r(hm, i);
    RETURN_IF(!e, ss_void);
    return sso1_get((const srt_stringo1 *)
                                              &e->v);
}
/* #API: |Enumerate map values (SHM_DS)|hash map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *shm_it_ds_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapDS *e;
    RETURN_IF(!hm || SHM_DS != hm->d.sub_type, ss_void);
    e = (const struct SHMapDS *)shm_enum_r(hm, i);
    RETURN_IF(!e, ss_void);
    return sso1_get((const srt_stringo1 *)
                                              &e->v);
}
/* #API: |Enumerate map values (SHM_IP)|hash map; element, 0 to n - 1|pointer|O(1)|1;2| */
S_INLINE const void *shm_it_ip_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapIP *e;
    RETURN_IF(!hm || SHM_IP != hm->d.sub_type, NULL);
    e = (const struct SHMapIP *)shm_enum_r(hm, i);
    RETURN_IF(!e, NULL);
    return e->v;
}
/* #API: |Enumerate map values (SHM_DP)|hash map; element, 0 to n - 1|pointer|O(1)|1;2| */
S_INLINE const void *shm_it_dp_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapDP *e;
    RETURN_IF(!hm || SHM_DP != hm->d.sub_type, NULL);
    e = (const struct SHMapDP *)shm_enum_r(hm, i);
    RETURN_IF(!e, NULL);
    return e->v;
}
/* #API: |Enumerate map values (SHM_SS)|hash map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *shm_it_ss_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapSS *e;
    RETURN_IF(!hm || SHM_SS != hm->d.sub_type, ss_void);
    e = (const struct SHMapSS *)shm_enum_r(hm, i);
    RETURN_IF(!e, ss_void);
    return sso_get_s2(&e->kv);
}
/* #API: |Enumerate map values (SHM_SP)|hash map; element, 0 to n - 1|pointer|O(1)|1;2| */
S_INLINE const void *shm_it_sp_v(const srt_hmap *hm, size_t i)
{
    const struct SHMapSP *e;
    RETURN_IF(!hm || SHM_SP != hm->d.sub_type, NULL);
    e = (const struct SHMapSP *)shm_enum_r(hm, i);
    RETURN_IF(!e, NULL);
    return e->v;
}

/*
 * Enumeration, with callback helper
 */

typedef srt_bool (*srt_hmap_it_ii32)(int32_t k, int32_t v, void *context);
typedef srt_bool (*srt_hmap_it_uu32)(uint32_t k, uint32_t v, void *context);
typedef srt_bool (*srt_hmap_it_ii)(int64_t k, int64_t v, void *context);
typedef srt_bool (*srt_hmap_it_ff)(float k, float v, void *context);
typedef srt_bool (*srt_hmap_it_dd)(double k, double v, void *context);
typedef srt_bool (*srt_hmap_it_si)(const srt_string* k, int64_t v, void *context);
typedef srt_bool (*srt_hmap_it_su)(const srt_string* k, uint64_t v, void *context);
typedef srt_bool (*srt_hmap_it_sd)(const srt_string* k, double v, void *context);
typedef srt_bool (*srt_hmap_it_is)(int64_t k, const srt_string* v, void *context);
typedef srt_bool (*srt_hmap_it_us)(uint64_t k, const srt_string* v, void *context);
typedef srt_bool (*srt_hmap_it_ds)(double k, const srt_string* v, void *context);
typedef srt_bool (*srt_hmap_it_ip)(int64_t k, const void* v, void *context);
typedef srt_bool (*srt_hmap_it_dp)(double k, const void* v, void *context);
typedef srt_bool (*srt_hmap_it_ss)(const srt_string* k, const srt_string* v, void *context);
typedef srt_bool (*srt_hmap_it_sp)(const srt_string* k, const void* v, void *context);

/* #API: |Enumerate map elements in portions (SHM_II32)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_ii32(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_ii32 f, void *context);
/* #API: |Enumerate map elements in portions (SHM_UU32)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_uu32(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_uu32 f, void *context);
/* #API: |Enumerate map elements in portions (SHM_II)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_ii(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_ii f, void *context);
/* #API: |Enumerate map elements in portions (SHM_FF)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_ff(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_ff f, void *context);
/* #API: |Enumerate map elements in portions (SHM_DD)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_dd(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_dd f, void *context);
/* #API: |Enumerate map elements in portions (SHM_SI)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_si(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_si f, void *context);
/* #API: |Enumerate map elements in portions (SHM_SU)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_su(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_su f, void *context);
/* #API: |Enumerate map elements in portions (SHM_SD)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_sd(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_sd f, void *context);
/* #API: |Enumerate map elements in portions (SHM_IS)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_is(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_is f, void *context);
/* #API: |Enumerate map elements in portions (SHM_US)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_us(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_us f, void *context);
/* #API: |Enumerate map elements in portions (SHM_DS)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_ds(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_ds f, void *context);
/* #API: |Enumerate map elements in portions (SHM_IP)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_ip(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_ip f, void *context);
/* #API: |Enumerate map elements in portions (SHM_DP)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_dp(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_dp f, void *context);
/* #API: |Enumerate map elements in portions (SHM_SS)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_ss(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_ss f, void *context);
/* #API: |Enumerate map elements in portions (SHM_SP)|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_sp(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_sp f, void *context);

#ifdef __cplusplus
} /* extern "C" { */
#endif
#endif /* #ifndef SHMAP_H */

