/*
 * smset.c
 *
 * Set handling.
 *
 * Copyright (c) 2015-2020 F. Aragon. All rights reserved.
 * Released under the BSD 3-Clause License (see the doc/LICENSE)
 */

#include "smset.h"
#include "saux/scommon.h"

size_t sms_itr_i32(const srt_map *m,
             int32_t kmin, int32_t kmax,
             srt_set_it_i32 f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM0_I32, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_ni_i((const struct SMapi *)cn, kmin);
            cmpmax = cmp_ni_i((const struct SMapi *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapi *)cn)->k, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_ni_i((const struct SMapi *)cn, kmin);
            cmpmax = cmp_ni_i((const struct SMapi *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapi *)cn)->k, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sms_itr_u32(const srt_map *m,
             uint32_t kmin, uint32_t kmax,
             srt_set_it_u32 f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM0_U32, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nu_u((const struct SMapu *)cn, kmin);
            cmpmax = cmp_nu_u((const struct SMapu *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapu *)cn)->k, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nu_u((const struct SMapu *)cn, kmin);
            cmpmax = cmp_nu_u((const struct SMapu *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapu *)cn)->k, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sms_itr_i(const srt_map *m,
             int64_t kmin, int64_t kmax,
             srt_set_it_i f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM0_I, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nI_I((const struct SMapI *)cn, kmin);
            cmpmax = cmp_nI_I((const struct SMapI *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapI *)cn)->k, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nI_I((const struct SMapI *)cn, kmin);
            cmpmax = cmp_nI_I((const struct SMapI *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapI *)cn)->k, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sms_itr_u(const srt_map *m,
             uint64_t kmin, uint64_t kmax,
             srt_set_it_u f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM0_U, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nU_U((const struct SMapU *)cn, kmin);
            cmpmax = cmp_nU_U((const struct SMapU *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapU *)cn)->k, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nU_U((const struct SMapU *)cn, kmin);
            cmpmax = cmp_nU_U((const struct SMapU *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapU *)cn)->k, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sms_itr_f(const srt_map *m,
             float kmin, float kmax,
             srt_set_it_f f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM0_F, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nF_F((const struct SMapF *)cn, kmin);
            cmpmax = cmp_nF_F((const struct SMapF *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapF *)cn)->k, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nF_F((const struct SMapF *)cn, kmin);
            cmpmax = cmp_nF_F((const struct SMapF *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapF *)cn)->k, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sms_itr_d(const srt_map *m,
             double kmin, double kmax,
             srt_set_it_d f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM0_D, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nD_D((const struct SMapD *)cn, kmin);
            cmpmax = cmp_nD_D((const struct SMapD *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapD *)cn)->k, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nD_D((const struct SMapD *)cn, kmin);
            cmpmax = cmp_nD_D((const struct SMapD *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapD *)cn)->k, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sms_itr_s(const srt_map *m,
             const srt_string* kmin, const srt_string* kmax,
             srt_set_it_s f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM0_S, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(sso_get((const srt_stringo *)
                                  &((const struct SMapS *)cn)->k), context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(sso_get((const srt_stringo *)
                                  &((const struct SMapS *)cn)->k), context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
