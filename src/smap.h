#ifndef SMAP_H
#define SMAP_H
#ifdef __cplusplus
extern "C" {
#endif


/*
 * smap.h
 *
 * #SHORTDOC map handling (key-value storage)
 *
 * #DOC Map functions handle key-value storage, which is implemented as a
 * #DOC Red-Black tree (O(log n) time complexity for insert/read/delete)
 * #DOC
 * #DOC
 * #DOC Supported key/value modes (enum eSM_Type):
 * #DOC
 * #DOC
 * #DOC SM_II32: int32_t key, int32_t value
 * #DOC
 * #DOC SM_UU32: uint32_t key, uint32_t value
 * #DOC
 * #DOC SM_II: int64_t key, int64_t value
 * #DOC
 * #DOC SM_FF: float key, float value
 * #DOC
 * #DOC SM_DD: double key, double value
 * #DOC
 * #DOC SM_SI: string key, int64_t value
 * #DOC
 * #DOC SM_SU: string key, uint64_t value
 * #DOC
 * #DOC SM_SD: string key, double value
 * #DOC
 * #DOC SM_IS: int64_t key, string value
 * #DOC
 * #DOC SM_US: uint64_t key, string value
 * #DOC
 * #DOC SM_DS: double key, string value
 * #DOC
 * #DOC SM_IP: int64_t key, pointer value
 * #DOC
 * #DOC SM_DP: double key, pointer value
 * #DOC
 * #DOC SM_SS: string key, string value
 * #DOC
 * #DOC SM_SP: string key, pointer value
 * #DOC
 * #DOC
 * #DOC Callback types for the sm_itr_*() functions:
 * #DOC
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_ii32)(int32_t k, int32_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_uu32)(uint32_t k, uint32_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_ii)(int64_t k, int64_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_ff)(float k, float v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_dd)(double k, double v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_si)(const srt_string* k, int64_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_su)(const srt_string* k, uint64_t v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_sd)(const srt_string* k, double v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_is)(int64_t k, const srt_string* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_us)(uint64_t k, const srt_string* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_ds)(double k, const srt_string* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_ip)(int64_t k, const void* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_dp)(double k, const void* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_ss)(const srt_string* k, const srt_string* v, void *context);
 * #DOC
 * #DOC typedef srt_bool (*srt_map_it_sp)(const srt_string* k, const void* v, void *context);
 *
 * Copyright (c) 2015-2020 F. Aragon. All rights reserved.
 * Released under the BSD 3-Clause License (see the doc/LICENSE)
 */

#include "saux/stree.h"
#include "saux/sstringo.h"
#include "sstring.h"
#include "svector.h"

/*
 * Structures
 */

enum eSM_Type0 {
    SM0_I32,
    SM0_U32,
    SM0_I,
    SM0_U,
    SM0_F,
    SM0_D,
    SM0_S,
    SM0_II32,
    SM0_UU32,
    SM0_II,
    SM0_FF,
    SM0_DD,
    SM0_SI,
    SM0_SU,
    SM0_SD,
    SM0_IS,
    SM0_US,
    SM0_DS,
    SM0_IP,
    SM0_DP,
    SM0_SS,
    SM0_SP,
    SM0_NumTypes
};

enum eSM_Type {
    SM_II32 = SM0_II32,
    SM_UU32 = SM0_UU32,
    SM_II = SM0_II,
    SM_FF = SM0_FF,
    SM_DD = SM0_DD,
    SM_SI = SM0_SI,
    SM_SU = SM0_SU,
    SM_SD = SM0_SD,
    SM_IS = SM0_IS,
    SM_US = SM0_US,
    SM_DS = SM0_DS,
    SM_IP = SM0_IP,
    SM_DP = SM0_DP,
    SM_SS = SM0_SS,
    SM_SP = SM0_SP,
};

struct SMapi {
    srt_tnode n;
    int32_t k;
};
struct SMapu {
    srt_tnode n;
    uint32_t k;
};
struct SMapI {
    srt_tnode n;
    int64_t k;
};
struct SMapU {
    srt_tnode n;
    uint64_t k;
};
struct SMapF {
    srt_tnode n;
    float k;
};
struct SMapD {
    srt_tnode n;
    double k;
};

struct SMapS {
    srt_tnode n;
    srt_stringo1 k;
};

struct SMapSS {
    srt_tnode n;
    srt_stringo s;
};

struct SMapii {
    struct SMapi x;
    int32_t v;
};
struct SMapuu {
    struct SMapu x;
    uint32_t v;
};
struct SMapII {
    struct SMapI x;
    int64_t v;
};
struct SMapFF {
    struct SMapF x;
    float v;
};
struct SMapDD {
    struct SMapD x;
    double v;
};

struct SMapIS {
    struct SMapI x;
    srt_stringo1 v;
};
struct SMapUS {
    struct SMapU x;
    srt_stringo1 v;
};
struct SMapDS {
    struct SMapD x;
    srt_stringo1 v;
};

struct SMapIP {
    struct SMapI x;
    const void *v;
};
struct SMapDP {
    struct SMapD x;
    const void *v;
};
struct SMapSP {
    struct SMapS x;
    const void *v;
};

struct SMapSI {
    struct SMapS x;
    int64_t v;
};
struct SMapSU {
    struct SMapS x;
    uint64_t v;
};
struct SMapSD {
    struct SMapS x;
    double v;
};

typedef srt_tree srt_map; /* Opaque structure (accessors are provided) */
              /* (map is implemented as a tree)      */

typedef srt_bool (*srt_map_it_ii32)(int32_t k, int32_t v, void *context);
typedef srt_bool (*srt_map_it_uu32)(uint32_t k, uint32_t v, void *context);
typedef srt_bool (*srt_map_it_ii)(int64_t k, int64_t v, void *context);
typedef srt_bool (*srt_map_it_ff)(float k, float v, void *context);
typedef srt_bool (*srt_map_it_dd)(double k, double v, void *context);
typedef srt_bool (*srt_map_it_si)(const srt_string* k, int64_t v, void *context);
typedef srt_bool (*srt_map_it_su)(const srt_string* k, uint64_t v, void *context);
typedef srt_bool (*srt_map_it_sd)(const srt_string* k, double v, void *context);
typedef srt_bool (*srt_map_it_is)(int64_t k, const srt_string* v, void *context);
typedef srt_bool (*srt_map_it_us)(uint64_t k, const srt_string* v, void *context);
typedef srt_bool (*srt_map_it_ds)(double k, const srt_string* v, void *context);
typedef srt_bool (*srt_map_it_ip)(int64_t k, const void* v, void *context);
typedef srt_bool (*srt_map_it_dp)(double k, const void* v, void *context);
typedef srt_bool (*srt_map_it_ss)(const srt_string* k, const srt_string* v, void *context);
typedef srt_bool (*srt_map_it_sp)(const srt_string* k, const void* v, void *context);

/*
 * Allocation
 */

/*
#API: |Allocate map (stack)|map type; initial reserve|map|O(1)|1;2|
srt_map *sm_alloca(enum eSM_Type t, size_t n);
*/
#define sm_alloca(type, max_size)                                      \
    sm_alloc_raw(type, S_TRUE,                                         \
             s_alloca(sd_alloc_size_raw(sizeof(srt_map),               \
                        sm_elem_size(type), max_size,  \
                        S_FALSE)),                     \
             sm_elem_size(type), max_size)

srt_map *sm_alloc_raw0(enum eSM_Type0 t, srt_bool ext_buf,
               void *buffer, size_t elem_size,
               size_t max_size);

S_INLINE srt_map *sm_alloc_raw(enum eSM_Type t, srt_bool ext_buf,
                   void *buffer, size_t elem_size,
                   size_t max_size)
{
    return sm_alloc_raw0((enum eSM_Type0)t, ext_buf, buffer, elem_size,
                 max_size);
}

srt_map *sm_alloc0(enum eSM_Type0 t, size_t initial_num_elems_reserve);

/* #API: |Allocate map (heap)|map type; initial reserve|map|O(1)|1;2| */
S_INLINE srt_map *sm_alloc(enum eSM_Type t, size_t initial_num_elems_reserve)
{
    return sm_alloc0((enum eSM_Type0)t, initial_num_elems_reserve);
}

/* #NOTAPI: |Get map node size from map type|map type|bytes required for storing a single node|O(1)|1;2| */
S_INLINE uint8_t sm_elem_size(int t)
{
    switch (t) {
        case SM0_I32: return sizeof(struct SMapi);
        case SM0_U32: return sizeof(struct SMapu);
        case SM0_I: return sizeof(struct SMapI);
        case SM0_U: return sizeof(struct SMapU);
        case SM0_F: return sizeof(struct SMapF);
        case SM0_D: return sizeof(struct SMapD);
        case SM0_S: return sizeof(struct SMapS);
        case SM0_II32: return sizeof(struct SMapii);
        case SM0_UU32: return sizeof(struct SMapuu);
        case SM0_II: return sizeof(struct SMapII);
        case SM0_FF: return sizeof(struct SMapFF);
        case SM0_DD: return sizeof(struct SMapDD);
        case SM0_SI: return sizeof(struct SMapSI);
        case SM0_SU: return sizeof(struct SMapSU);
        case SM0_SD: return sizeof(struct SMapSD);
        case SM0_IS: return sizeof(struct SMapIS);
        case SM0_US: return sizeof(struct SMapUS);
        case SM0_DS: return sizeof(struct SMapDS);
        case SM0_IP: return sizeof(struct SMapIP);
        case SM0_DP: return sizeof(struct SMapDP);
        case SM0_SS: return sizeof(struct SMapSS);
        case SM0_SP: return sizeof(struct SMapSP);
        default: break;
    }
    return 0;
}

/* #API: |Duplicate map|input map|output map|O(n)|1;2| */
srt_map *sm_dup(const srt_map *src);

/* #API: |Reset/clean map (keeping map type)|map|-|O(1) for simple maps, O(n) for maps having nodes with strings|1;2| */
void sm_clear(srt_map *m);

/*
#API: |Free one or more maps (heap)|map; more maps (optional)|-|O(1) for simple maps, O(n) for maps having nodes with strings|1;2|
void sm_free(srt_map **m, ...)
*/
#ifdef S_USE_VA_ARGS
#define sm_free(...) sm_free_aux(__VA_ARGS__, S_INVALID_PTR_VARG_TAIL)
#else
#define sm_free(m) sm_free_aux(m, S_INVALID_PTR_VARG_TAIL)
#endif
void sm_free_aux(srt_map **m, ...);

S_INLINE size_t sm_size(const srt_map *c)
{
    return sd_size((const srt_data *)c);
}

S_INLINE void sm_set_size(srt_map *c, size_t s)
{
    sd_set_size((srt_data *)c, s);
}

S_INLINE size_t sm_max_size(const srt_map *c)
{
    return sd_max_size((const srt_data *)c);
}

S_INLINE size_t sm_alloc_size(const srt_map *c)
{
    return sd_alloc_size((const srt_data *)c);
}

S_INLINE size_t sm_get_buffer_size(const srt_map *c)
{
    return sd_size((const srt_data *)c)
         * sd_elem_size((const srt_data *)c);
}

S_INLINE void *sm_elem_addr(srt_map *c, size_t pos)
{
    return sd_elem_addr((srt_data *)c, pos);
}

S_INLINE const void *sm_elem_addr_r(const srt_map *c, size_t pos)
{
    return sd_elem_addr_r((const srt_data *)c, pos);
}

S_INLINE size_t sm_capacity(const srt_map *c)
{
    return sd_max_size((const srt_data *)c);
}

S_INLINE size_t sm_capacity_left(const srt_map *c)
{
    return sd_max_size((const srt_data *)c)
         - sd_size((const srt_data *)c);
}

S_INLINE size_t sm_len(const srt_map *c)
{
    return sd_size((const srt_data *)c);
}
S_INLINE uint8_t *sm_get_buffer(srt_map *c)
{
    return sd_get_buffer((srt_data *)c);
}

S_INLINE const uint8_t *sm_get_buffer_r(const srt_map *c)
{
    return sd_get_buffer_r((const srt_data *)c);
}
S_INLINE srt_map *sm_shrink(srt_map **c)
{
    return (srt_map *)sd_shrink((srt_data **)c, 0);
}

S_INLINE srt_bool sm_empty(const srt_map *c)
{
    return sm_size(c) == 0 ? S_TRUE : S_FALSE;
}

S_INLINE void sm_set_alloc_errors(srt_map *c)
{
    sd_set_alloc_errors((srt_data *)c);
}

S_INLINE void sm_reset_alloc_errors(srt_map *c)
{
    sd_reset_alloc_errors((srt_data *)c);
}

S_INLINE srt_bool sm_alloc_errors(srt_map *c)
{
    return sd_alloc_errors((srt_data *)c);
}

S_INLINE size_t sm_grow(srt_map **c, size_t extra_elems)
{
    return sd_grow((srt_data **)c, extra_elems, 0);
}

S_INLINE size_t sm_reserve(srt_map **c, size_t max_elems)
{
    return sd_reserve((srt_data **)c, max_elems, 0);
}

/*
#API: |Ensure space for extra elements|map;number of extra elements|extra size allocated|O(1)|1;2|
size_t sm_grow(srt_map **m, size_t extra_elems)

#API: |Ensure space for elements|map;absolute element reserve|reserved elements|O(1)|1;2|
size_t sm_reserve(srt_map **m, size_t max_elems)

#API: |Make the map use the minimum possible memory|map|map reference (optional usage)|O(1) for allocators using memory remap; O(n) for naive allocators|1;2|
srt_map *sm_shrink(srt_map **m);

#API: |Get map size|map|Map number of elements|O(1)|1;2|
size_t sm_size(const srt_map *m);

#API: |Allocated space|map|current allocated space (vector elements)|O(1)|1;2|
size_t sm_capacity(const srt_map *m);

#API: |Preallocated space left|map|allocated space left|O(1)|1;2|
size_t sm_capacity_left(const srt_map *m);

#API: |Tells if a map is empty (zero elements)|map|S_TRUE: empty; S_FALSE: not empty|O(1)|1;2|
srt_bool sm_empty(const srt_map *m)
*/

/*
 * Copy
 */

/* #API: |Overwrite map with a map copy|output map; input map|output map reference (optional usage)|O(n)|1;2| */
srt_map *sm_cpy(srt_map **m, const srt_map *src);

/*
 * Random access
 */

/* #API: |Access to map element (SM_II32)|map; key|value|O(log n)|1;2| */
int32_t sm_at_ii32(const srt_map *m, int32_t k);
/* #API: |Access to map element (SM_UU32)|map; key|value|O(log n)|1;2| */
uint32_t sm_at_uu32(const srt_map *m, uint32_t k);
/* #API: |Access to map element (SM_II)|map; key|value|O(log n)|1;2| */
int64_t sm_at_ii(const srt_map *m, int64_t k);
/* #API: |Access to map element (SM_FF)|map; key|value|O(log n)|1;2| */
float sm_at_ff(const srt_map *m, float k);
/* #API: |Access to map element (SM_DD)|map; key|value|O(log n)|1;2| */
double sm_at_dd(const srt_map *m, double k);
/* #API: |Access to map element (SM_SI)|map; key|value|O(log n)|1;2| */
int64_t sm_at_si(const srt_map *m, const srt_string* k);
/* #API: |Access to map element (SM_SU)|map; key|value|O(log n)|1;2| */
uint64_t sm_at_su(const srt_map *m, const srt_string* k);
/* #API: |Access to map element (SM_SD)|map; key|value|O(log n)|1;2| */
double sm_at_sd(const srt_map *m, const srt_string* k);
/* #API: |Access to map element (SM_IS)|map; key|value|O(log n)|1;2| */
const srt_string *sm_at_is(const srt_map *m, int64_t k);
/* #API: |Access to map element (SM_US)|map; key|value|O(log n)|1;2| */
const srt_string *sm_at_us(const srt_map *m, uint64_t k);
/* #API: |Access to map element (SM_DS)|map; key|value|O(log n)|1;2| */
const srt_string *sm_at_ds(const srt_map *m, double k);
/* #API: |Access to map element (SM_IP)|map; key|value|O(log n)|1;2| */
const void *sm_at_ip(const srt_map *m, int64_t k);
/* #API: |Access to map element (SM_DP)|map; key|value|O(log n)|1;2| */
const void *sm_at_dp(const srt_map *m, double k);
/* #API: |Access to map element (SM_SS)|map; key|value|O(log n)|1;2| */
const srt_string *sm_at_ss(const srt_map *m, const srt_string* k);
/* #API: |Access to map element (SM_SP)|map; key|value|O(log n)|1;2| */
const void *sm_at_sp(const srt_map *m, const srt_string* k);

/*
 * Existence check
 */

/* #API: |Map element count/check (SM_I*32)|map; key|S_TRUE: element found; S_FALSE: not in the map|O(log n)|1;2| */
size_t sm_count_i32(const srt_map *m, int32_t k);
/* #API: |Map element count/check (SM_U*32)|map; key|S_TRUE: element found; S_FALSE: not in the map|O(log n)|1;2| */
size_t sm_count_u32(const srt_map *m, uint32_t k);
/* #API: |Map element count/check (SM_I*)|map; key|S_TRUE: element found; S_FALSE: not in the map|O(log n)|1;2| */
size_t sm_count_i(const srt_map *m, int64_t k);
/* #API: |Map element count/check (SM_U*)|map; key|S_TRUE: element found; S_FALSE: not in the map|O(log n)|1;2| */
size_t sm_count_u(const srt_map *m, uint64_t k);
/* #API: |Map element count/check (SM_F*)|map; key|S_TRUE: element found; S_FALSE: not in the map|O(log n)|1;2| */
size_t sm_count_f(const srt_map *m, float k);
/* #API: |Map element count/check (SM_D*)|map; key|S_TRUE: element found; S_FALSE: not in the map|O(log n)|1;2| */
size_t sm_count_d(const srt_map *m, double k);
/* #API: |Map element count/check (SM_S*)|map; key|S_TRUE: element found; S_FALSE: not in the map|O(log n)|1;2| */
size_t sm_count_s(const srt_map *m, const srt_string* k);

/*
 * Insert
 */

/* #API: |Insert map element (SM_II32)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_ii32(srt_map **m, int32_t k, int32_t v);
/* #API: |Insert map element (SM_UU32)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_uu32(srt_map **m, uint32_t k, uint32_t v);
/* #API: |Insert map element (SM_II)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_ii(srt_map **m, int64_t k, int64_t v);
/* #API: |Insert map element (SM_FF)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_ff(srt_map **m, float k, float v);
/* #API: |Insert map element (SM_DD)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_dd(srt_map **m, double k, double v);
/* #API: |Insert map element (SM_SI)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_si(srt_map **m, const srt_string* k, int64_t v);
/* #API: |Insert map element (SM_SU)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_su(srt_map **m, const srt_string* k, uint64_t v);
/* #API: |Insert map element (SM_SD)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_sd(srt_map **m, const srt_string* k, double v);
/* #API: |Insert map element (SM_IS)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_is(srt_map **m, int64_t k, const srt_string* v);
/* #API: |Insert map element (SM_US)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_us(srt_map **m, uint64_t k, const srt_string* v);
/* #API: |Insert map element (SM_DS)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_ds(srt_map **m, double k, const srt_string* v);
/* #API: |Insert map element (SM_IP)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_ip(srt_map **m, int64_t k, const void* v);
/* #API: |Insert map element (SM_DP)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_dp(srt_map **m, double k, const void* v);
/* #API: |Insert map element (SM_SS)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_ss(srt_map **m, const srt_string* k, const srt_string* v);
/* #API: |Insert map element (SM_SP)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_insert_sp(srt_map **m, const srt_string* k, const void* v);

/*
 * Increment
 */

/* #API: |Increment map element (SM_II32)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_inc_ii32(srt_map **m, int32_t k, int32_t v);
/* #API: |Increment map element (SM_UU32)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_inc_uu32(srt_map **m, uint32_t k, uint32_t v);
/* #API: |Increment map element (SM_II)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_inc_ii(srt_map **m, int64_t k, int64_t v);
/* #API: |Increment map element (SM_FF)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_inc_ff(srt_map **m, float k, float v);
/* #API: |Increment map element (SM_DD)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_inc_dd(srt_map **m, double k, double v);
/* #API: |Increment map element (SM_SI)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_inc_si(srt_map **m, const srt_string* k, int64_t v);
/* #API: |Increment map element (SM_SU)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_inc_su(srt_map **m, const srt_string* k, uint64_t v);
/* #API: |Increment map element (SM_SD)|map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(log n)|1;2| */
srt_bool sm_inc_sd(srt_map **m, const srt_string* k, double v);

/*
 * Delete
 */

/* #API: |Delete map element (SM_I*32)|map; key|S_TRUE: found and deleted; S_FALSE: not found|O(log n)|1;2| */
srt_bool sm_delete_i32(srt_map *m, int32_t k);
/* #API: |Delete map element (SM_U*32)|map; key|S_TRUE: found and deleted; S_FALSE: not found|O(log n)|1;2| */
srt_bool sm_delete_u32(srt_map *m, uint32_t k);
/* #API: |Delete map element (SM_I*)|map; key|S_TRUE: found and deleted; S_FALSE: not found|O(log n)|1;2| */
srt_bool sm_delete_i(srt_map *m, int64_t k);
/* #API: |Delete map element (SM_U*)|map; key|S_TRUE: found and deleted; S_FALSE: not found|O(log n)|1;2| */
srt_bool sm_delete_u(srt_map *m, uint64_t k);
/* #API: |Delete map element (SM_F*)|map; key|S_TRUE: found and deleted; S_FALSE: not found|O(log n)|1;2| */
srt_bool sm_delete_f(srt_map *m, float k);
/* #API: |Delete map element (SM_D*)|map; key|S_TRUE: found and deleted; S_FALSE: not found|O(log n)|1;2| */
srt_bool sm_delete_d(srt_map *m, double k);
/* #API: |Delete map element (SM_S*)|map; key|S_TRUE: found and deleted; S_FALSE: not found|O(log n)|1;2| */
srt_bool sm_delete_s(srt_map *m, const srt_string* k);

/*
 * Enumeration / export data
 */

/* #API: |Enumerate map elements in a given key range (SM_II32)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_ii32(const srt_map *m, int32_t key_min, int32_t key_max, srt_map_it_ii32 f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_UU32)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_uu32(const srt_map *m, uint32_t key_min, uint32_t key_max, srt_map_it_uu32 f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_II)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_ii(const srt_map *m, int64_t key_min, int64_t key_max, srt_map_it_ii f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_FF)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_ff(const srt_map *m, float key_min, float key_max, srt_map_it_ff f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_DD)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_dd(const srt_map *m, double key_min, double key_max, srt_map_it_dd f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_SI)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_si(const srt_map *m, const srt_string* key_min, const srt_string* key_max, srt_map_it_si f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_SU)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_su(const srt_map *m, const srt_string* key_min, const srt_string* key_max, srt_map_it_su f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_SD)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_sd(const srt_map *m, const srt_string* key_min, const srt_string* key_max, srt_map_it_sd f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_IS)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_is(const srt_map *m, int64_t key_min, int64_t key_max, srt_map_it_is f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_US)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_us(const srt_map *m, uint64_t key_min, uint64_t key_max, srt_map_it_us f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_DS)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_ds(const srt_map *m, double key_min, double key_max, srt_map_it_ds f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_IP)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_ip(const srt_map *m, int64_t key_min, int64_t key_max, srt_map_it_ip f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_DP)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_dp(const srt_map *m, double key_min, double key_max, srt_map_it_dp f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_SS)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_ss(const srt_map *m, const srt_string* key_min, const srt_string* key_max, srt_map_it_ss f, void *context);
/* #API: |Enumerate map elements in a given key range (SM_SP)|map; key lower bound; key upper bound; callback function; callback function context|Elements processed|O(log n) + O(log m); additional 2 * O(log n) space required, allocated on the stack, i.e. fast|1;2| */
size_t sm_itr_sp(const srt_map *m, const srt_string* key_min, const srt_string* key_max, srt_map_it_sp f, void *context);

/* #NOTAPI: |Sort map to vector (used for test coverage, not as documented API)|map; output vector for keys; output vector for values|Number of map elements|O(n)|0;1| */
ssize_t sm_sort_to_vectors(const srt_map *m, srt_vector **kv, srt_vector **vv);

/*
 * Auxiliary inlined functions
 */

    /*
     * Unordered enumeration is inlined in order to get almost as fast
     * as array access after compiler optimization.
     */

/* #API: |Enumerate map keys (SM_I*32)|map; element, 0 to n - 1|int32_t|O(1)|1;2| */
S_INLINE int32_t sm_it_i32_k(const srt_map *m, srt_tndx i)
{
    const struct SMapi *n = (const struct SMapi *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SM_U*32)|map; element, 0 to n - 1|uint32_t|O(1)|1;2| */
S_INLINE uint32_t sm_it_u32_k(const srt_map *m, srt_tndx i)
{
    const struct SMapu *n = (const struct SMapu *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SM_I*)|map; element, 0 to n - 1|int64_t|O(1)|1;2| */
S_INLINE int64_t sm_it_i_k(const srt_map *m, srt_tndx i)
{
    const struct SMapI *n = (const struct SMapI *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SM_U*)|map; element, 0 to n - 1|uint64_t|O(1)|1;2| */
S_INLINE uint64_t sm_it_u_k(const srt_map *m, srt_tndx i)
{
    const struct SMapU *n = (const struct SMapU *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SM_F*)|map; element, 0 to n - 1|float|O(1)|1;2| */
S_INLINE float sm_it_f_k(const srt_map *m, srt_tndx i)
{
    const struct SMapF *n = (const struct SMapF *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SM_D*)|map; element, 0 to n - 1|double|O(1)|1;2| */
S_INLINE double sm_it_d_k(const srt_map *m, srt_tndx i)
{
    const struct SMapD *n = (const struct SMapD *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->k;
}
/* #API: |Enumerate map keys (SM_S*)|map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *sm_it_s_k(const srt_map *m, srt_tndx i)
{
    const struct SMapS *n = (const struct SMapS *)st_enum_r(m, i);
    RETURN_IF(!n, ss_void);
    return sso_get((const srt_stringo *)
                                  &n->k);
}

/* #API: |Enumerate map values (SM_II32)|map; element, 0 to n - 1|int32_t|O(1)|1;2| */
S_INLINE int32_t sm_it_ii32_v(const srt_map *m, srt_tndx i)
{
    const struct SMapii *n;
    RETURN_IF(!m || SM_II32 != m->d.sub_type, 0);
    n = (const struct SMapii *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->v;
}
/* #API: |Enumerate map values (SM_UU32)|map; element, 0 to n - 1|uint32_t|O(1)|1;2| */
S_INLINE uint32_t sm_it_uu32_v(const srt_map *m, srt_tndx i)
{
    const struct SMapuu *n;
    RETURN_IF(!m || SM_UU32 != m->d.sub_type, 0);
    n = (const struct SMapuu *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->v;
}
/* #API: |Enumerate map values (SM_II)|map; element, 0 to n - 1|int64_t|O(1)|1;2| */
S_INLINE int64_t sm_it_ii_v(const srt_map *m, srt_tndx i)
{
    const struct SMapII *n;
    RETURN_IF(!m || SM_II != m->d.sub_type, 0);
    n = (const struct SMapII *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->v;
}
/* #API: |Enumerate map values (SM_FF)|map; element, 0 to n - 1|float|O(1)|1;2| */
S_INLINE float sm_it_ff_v(const srt_map *m, srt_tndx i)
{
    const struct SMapFF *n;
    RETURN_IF(!m || SM_FF != m->d.sub_type, 0);
    n = (const struct SMapFF *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->v;
}
/* #API: |Enumerate map values (SM_DD)|map; element, 0 to n - 1|double|O(1)|1;2| */
S_INLINE double sm_it_dd_v(const srt_map *m, srt_tndx i)
{
    const struct SMapDD *n;
    RETURN_IF(!m || SM_DD != m->d.sub_type, 0);
    n = (const struct SMapDD *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->v;
}
/* #API: |Enumerate map values (SM_SI)|map; element, 0 to n - 1|int64_t|O(1)|1;2| */
S_INLINE int64_t sm_it_si_v(const srt_map *m, srt_tndx i)
{
    const struct SMapSI *n;
    RETURN_IF(!m || SM_SI != m->d.sub_type, 0);
    n = (const struct SMapSI *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->v;
}
/* #API: |Enumerate map values (SM_SU)|map; element, 0 to n - 1|uint64_t|O(1)|1;2| */
S_INLINE uint64_t sm_it_su_v(const srt_map *m, srt_tndx i)
{
    const struct SMapSU *n;
    RETURN_IF(!m || SM_SU != m->d.sub_type, 0);
    n = (const struct SMapSU *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->v;
}
/* #API: |Enumerate map values (SM_SD)|map; element, 0 to n - 1|double|O(1)|1;2| */
S_INLINE double sm_it_sd_v(const srt_map *m, srt_tndx i)
{
    const struct SMapSD *n;
    RETURN_IF(!m || SM_SD != m->d.sub_type, 0);
    n = (const struct SMapSD *)st_enum_r(m, i);
    RETURN_IF(!n, 0);
    return n->v;
}
/* #API: |Enumerate map values (SM_IS)|map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *sm_it_is_v(const srt_map *m, srt_tndx i)
{
    const struct SMapIS *n;
    RETURN_IF(!m || SM_IS != m->d.sub_type, ss_void);
    n = (const struct SMapIS *)st_enum_r(m, i);
    RETURN_IF(!n, ss_void);
    return sso1_get(&n->v);
}
/* #API: |Enumerate map values (SM_US)|map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *sm_it_us_v(const srt_map *m, srt_tndx i)
{
    const struct SMapUS *n;
    RETURN_IF(!m || SM_US != m->d.sub_type, ss_void);
    n = (const struct SMapUS *)st_enum_r(m, i);
    RETURN_IF(!n, ss_void);
    return sso1_get(&n->v);
}
/* #API: |Enumerate map values (SM_DS)|map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *sm_it_ds_v(const srt_map *m, srt_tndx i)
{
    const struct SMapDS *n;
    RETURN_IF(!m || SM_DS != m->d.sub_type, ss_void);
    n = (const struct SMapDS *)st_enum_r(m, i);
    RETURN_IF(!n, ss_void);
    return sso1_get(&n->v);
}
/* #API: |Enumerate map values (SM_IP)|map; element, 0 to n - 1|pointer|O(1)|1;2| */
S_INLINE const void *sm_it_ip_v(const srt_map *m, srt_tndx i)
{
    const struct SMapIP *n;
    RETURN_IF(!m || SM_IP != m->d.sub_type, NULL);
    n = (const struct SMapIP *)st_enum_r(m, i);
    RETURN_IF(!n, NULL);
    return n->v;
}
/* #API: |Enumerate map values (SM_DP)|map; element, 0 to n - 1|pointer|O(1)|1;2| */
S_INLINE const void *sm_it_dp_v(const srt_map *m, srt_tndx i)
{
    const struct SMapDP *n;
    RETURN_IF(!m || SM_DP != m->d.sub_type, NULL);
    n = (const struct SMapDP *)st_enum_r(m, i);
    RETURN_IF(!n, NULL);
    return n->v;
}
/* #API: |Enumerate map values (SM_SS)|map; element, 0 to n - 1|string|O(1)|1;2| */
S_INLINE const srt_string *sm_it_ss_v(const srt_map *m, srt_tndx i)
{
    const struct SMapSS *n;
    RETURN_IF(!m || SM_SS != m->d.sub_type, ss_void);
    n = (const struct SMapSS *)st_enum_r(m, i);
    RETURN_IF(!n, ss_void);
    return sso_get_s2(&n->s);
}
/* #API: |Enumerate map values (SM_SP)|map; element, 0 to n - 1|pointer|O(1)|1;2| */
S_INLINE const void *sm_it_sp_v(const srt_map *m, srt_tndx i)
{
    const struct SMapSP *n;
    RETURN_IF(!m || SM_SP != m->d.sub_type, NULL);
    n = (const struct SMapSP *)st_enum_r(m, i);
    RETURN_IF(!n, NULL);
    return n->v;
}

/*
 * Comparison functions
 */

S_INLINE int cmp_ni_i(const struct SMapi *a, int32_t b) {
    return a ? (a->k > b ? 1 : a->k < b ? -1 : 0) : 0;
}
S_INLINE int cmp_nu_u(const struct SMapu *a, uint32_t b) {
    return a ? (a->k > b ? 1 : a->k < b ? -1 : 0) : 0;
}
S_INLINE int cmp_nI_I(const struct SMapI *a, int64_t b) {
    return a ? (a->k > b ? 1 : a->k < b ? -1 : 0) : 0;
}
S_INLINE int cmp_nU_U(const struct SMapU *a, uint64_t b) {
    return a ? (a->k > b ? 1 : a->k < b ? -1 : 0) : 0;
}
S_INLINE int cmp_nF_F(const struct SMapF *a, float b) {
    return a ? (a->k > b ? 1 : a->k < b ? -1 : 0) : 0;
}
S_INLINE int cmp_nD_D(const struct SMapD *a, double b) {
    return a ? (a->k > b ? 1 : a->k < b ? -1 : 0) : 0;
}

S_INLINE int cmp_nS_S(const struct SMapS *a, const srt_string *b)
{
    return ss_cmp(sso_get((const srt_stringo *)&a->k), b);
}

#ifdef __cplusplus
} /* extern "C" { */
#endif

#endif /* #ifndef SMAP_H */
