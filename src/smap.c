/*
 * smap.c
 *
 * Map handling.
 *
 * Copyright (c) 2015-2020 F. Aragon. All rights reserved.
 * Released under the BSD 3-Clause License (see the doc/LICENSE)
 */

#include "smap.h"
#include "saux/scommon.h"

/*
 * Internal constants
 */

struct SMapCtx {
    enum eSV_Type sort_kt, sort_vt;
    st_traverse sort_tr;
    srt_tree_callback delete_callback;
    srt_cmp cmpf;
};

/*
 * Internal functions
 */

static int cmp_i(const struct SMapi *a, const struct SMapi *b)
{
    return a->k > b->k ? 1 : a->k < b->k ? -1 : 0;
}
static int cmp_u(const struct SMapu *a, const struct SMapu *b)
{
    return a->k > b->k ? 1 : a->k < b->k ? -1 : 0;
}
static int cmp_I(const struct SMapI *a, const struct SMapI *b)
{
    return a->k > b->k ? 1 : a->k < b->k ? -1 : 0;
}
static int cmp_U(const struct SMapU *a, const struct SMapU *b)
{
    return a->k > b->k ? 1 : a->k < b->k ? -1 : 0;
}
static int cmp_F(const struct SMapF *a, const struct SMapF *b)
{
    return a->k > b->k ? 1 : a->k < b->k ? -1 : 0;
}
static int cmp_D(const struct SMapD *a, const struct SMapD *b)
{
    return a->k > b->k ? 1 : a->k < b->k ? -1 : 0;
}

static int cmp_S(const struct SMapS *a, const struct SMapS *b)
{
    return ss_cmp(sso_get((const srt_stringo *)&a->k),
                  sso_get((const srt_stringo *)&b->k));
}

static void rw_inc_SM_II32(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    if (existing)
        ((struct SMapii *)node)->v += ((const struct SMapii *)new_data)->v;
    else
        ((struct SMapii *)node)->v = ((const struct SMapii *)new_data)->v;
    ((struct SMapii *)node)->x.k = ((const struct SMapii *)new_data)->x.k;
}
static void rw_inc_SM_UU32(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    if (existing)
        ((struct SMapuu *)node)->v += ((const struct SMapuu *)new_data)->v;
    else
        ((struct SMapuu *)node)->v = ((const struct SMapuu *)new_data)->v;
    ((struct SMapuu *)node)->x.k = ((const struct SMapuu *)new_data)->x.k;
}
static void rw_inc_SM_II(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    if (existing)
        ((struct SMapII *)node)->v += ((const struct SMapII *)new_data)->v;
    else
        ((struct SMapII *)node)->v = ((const struct SMapII *)new_data)->v;
    ((struct SMapII *)node)->x.k = ((const struct SMapII *)new_data)->x.k;
}
static void rw_inc_SM_FF(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    if (existing)
        ((struct SMapFF *)node)->v += ((const struct SMapFF *)new_data)->v;
    else
        ((struct SMapFF *)node)->v = ((const struct SMapFF *)new_data)->v;
    ((struct SMapFF *)node)->x.k = ((const struct SMapFF *)new_data)->x.k;
}
static void rw_inc_SM_DD(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    if (existing)
        ((struct SMapDD *)node)->v += ((const struct SMapDD *)new_data)->v;
    else
        ((struct SMapDD *)node)->v = ((const struct SMapDD *)new_data)->v;
    ((struct SMapDD *)node)->x.k = ((const struct SMapDD *)new_data)->x.k;
}

static void rw_add_SM_S(srt_tnode *node, const srt_tnode *new_data,
                        srt_bool existing)
{
    struct SMapS *n = (struct SMapS *)node;
    const struct SMapS *m = (const struct SMapS *)new_data;
    if (!existing)
        sso1_set(&n->k, sso_get((const srt_stringo *)&m->k));
    else
        sso1_update(&n->k, sso_get((const srt_stringo *)&m->k));
}

static void rw_add_SM_SI(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    struct SMapSI *n = (struct SMapSI *)node;
    const struct SMapSI *m = (const struct SMapSI *)new_data;
    rw_add_SM_S(node, new_data, existing);
    n->v = m->v;
}
static void rw_add_SM_SU(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    struct SMapSU *n = (struct SMapSU *)node;
    const struct SMapSU *m = (const struct SMapSU *)new_data;
    rw_add_SM_S(node, new_data, existing);
    n->v = m->v;
}
static void rw_add_SM_SD(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    struct SMapSD *n = (struct SMapSD *)node;
    const struct SMapSD *m = (const struct SMapSD *)new_data;
    rw_add_SM_S(node, new_data, existing);
    n->v = m->v;
}
static void rw_add_SM_SP(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    struct SMapSP *n = (struct SMapSP *)node;
    const struct SMapSP *m = (const struct SMapSP *)new_data;
    rw_add_SM_S(node, new_data, existing);
    n->v = m->v;
}

static void rw_add_SM_SS(srt_tnode *node, const srt_tnode *new_data,
             srt_bool existing)
{
    struct SMapSS *n = (struct SMapSS *)node;
    const struct SMapSS *m = (const struct SMapSS *)new_data;
    if (!existing)
        sso_set(&n->s, sso_get(&m->s), sso_get_s2(&m->s));
    else
        sso_update(&n->s, sso_get(&m->s), sso_get_s2(&m->s));
}

static void rw_add_SM_IS(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    struct SMapIS *n = (struct SMapIS *)node;
    const struct SMapIS *nd = (const struct SMapIS *)new_data;
    n->x.k = nd->x.k;
    if (!existing)
        sso1_set(&n->v, sso1_get(&nd->v));
    else
        sso1_update(&n->v, sso1_get(&nd->v));
}
static void rw_add_SM_US(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    struct SMapUS *n = (struct SMapUS *)node;
    const struct SMapUS *nd = (const struct SMapUS *)new_data;
    n->x.k = nd->x.k;
    if (!existing)
        sso1_set(&n->v, sso1_get(&nd->v));
    else
        sso1_update(&n->v, sso1_get(&nd->v));
}
static void rw_add_SM_DS(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    struct SMapDS *n = (struct SMapDS *)node;
    const struct SMapDS *nd = (const struct SMapDS *)new_data;
    n->x.k = nd->x.k;
    if (!existing)
        sso1_set(&n->v, sso1_get(&nd->v));
    else
        sso1_update(&n->v, sso1_get(&nd->v));
}

static void rw_inc_SM_SI(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    if (!existing)
        rw_add_SM_SI(node, new_data, existing);
    else
        ((struct SMapSI *)node)->v += ((const struct SMapSI *)new_data)->v;
}
static void rw_inc_SM_SU(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    if (!existing)
        rw_add_SM_SU(node, new_data, existing);
    else
        ((struct SMapSU *)node)->v += ((const struct SMapSU *)new_data)->v;
}
static void rw_inc_SM_SD(srt_tnode *node, const srt_tnode *new_data,
                               srt_bool existing)
{
    if (!existing)
        rw_add_SM_SD(node, new_data, existing);
    else
        ((struct SMapSD *)node)->v += ((const struct SMapSD *)new_data)->v;
}

static void aux_is_delete(void *node)
{
    sso1_free(&((struct SMapIS *)node)->v);
}
static void aux_us_delete(void *node)
{
    sso1_free(&((struct SMapUS *)node)->v);
}
static void aux_ds_delete(void *node)
{
    sso1_free(&((struct SMapDS *)node)->v);
}

    /* clang-format off */

static void aux_sx_delete(void *node)
{
    sso1_free(&((struct SMapS *)node)->k);
}

static void aux_ss_delete(void *node)
{
    sso_free(&((struct SMapSS *)node)->s);
}

    /* clang-format on */

struct SV2X {
    srt_vector *kv, *vv;
};

static int aux_i32_sort(struct STraverseParams *tp)
{
    const struct SMapi *cn = (const struct SMapi *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_i32(&v2x->kv, cn->k);
    }
    return 0;
}
static int aux_u32_sort(struct STraverseParams *tp)
{
    const struct SMapu *cn = (const struct SMapu *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_u32(&v2x->kv, cn->k);
    }
    return 0;
}
static int aux_i_sort(struct STraverseParams *tp)
{
    const struct SMapI *cn = (const struct SMapI *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_i64(&v2x->kv, cn->k);
    }
    return 0;
}
static int aux_u_sort(struct STraverseParams *tp)
{
    const struct SMapU *cn = (const struct SMapU *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_u64(&v2x->kv, cn->k);
    }
    return 0;
}
static int aux_f_sort(struct STraverseParams *tp)
{
    const struct SMapF *cn = (const struct SMapF *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_f(&v2x->kv, cn->k);
    }
    return 0;
}
static int aux_d_sort(struct STraverseParams *tp)
{
    const struct SMapD *cn = (const struct SMapD *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_d(&v2x->kv, cn->k);
    }
    return 0;
}
static int aux_s_sort(struct STraverseParams *tp)
{
    const struct SMapS *cn = (const struct SMapS *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_gen(&v2x->kv, sso1_get(&cn->k));
    }
    return 0;
}

static int aux_ii32_sort(struct STraverseParams *tp)
{
    const struct SMapii *cn = (const struct SMapii *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_i32(&v2x->kv, cn->x.k);
        sv_push_i32(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_uu32_sort(struct STraverseParams *tp)
{
    const struct SMapuu *cn = (const struct SMapuu *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_u32(&v2x->kv, cn->x.k);
        sv_push_u32(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_ii_sort(struct STraverseParams *tp)
{
    const struct SMapII *cn = (const struct SMapII *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_i64(&v2x->kv, cn->x.k);
        sv_push_i64(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_ff_sort(struct STraverseParams *tp)
{
    const struct SMapFF *cn = (const struct SMapFF *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_f(&v2x->kv, cn->x.k);
        sv_push_f(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_dd_sort(struct STraverseParams *tp)
{
    const struct SMapDD *cn = (const struct SMapDD *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_d(&v2x->kv, cn->x.k);
        sv_push_d(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_si_sort(struct STraverseParams *tp)
{
    const struct SMapSI *cn = (const struct SMapSI *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_gen(&v2x->kv, sso1_get(&cn->x.k));
        sv_push_i64(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_su_sort(struct STraverseParams *tp)
{
    const struct SMapSU *cn = (const struct SMapSU *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_gen(&v2x->kv, sso1_get(&cn->x.k));
        sv_push_u64(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_sd_sort(struct STraverseParams *tp)
{
    const struct SMapSD *cn = (const struct SMapSD *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_gen(&v2x->kv, sso1_get(&cn->x.k));
        sv_push_d(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_is_sort(struct STraverseParams *tp)
{
    const struct SMapIS *cn = (const struct SMapIS *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_i64(&v2x->kv, cn->x.k);
        sv_push_gen(&v2x->vv, sso1_get(&cn->v));
    }
    return 0;
}
static int aux_us_sort(struct STraverseParams *tp)
{
    const struct SMapUS *cn = (const struct SMapUS *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_u64(&v2x->kv, cn->x.k);
        sv_push_gen(&v2x->vv, sso1_get(&cn->v));
    }
    return 0;
}
static int aux_ds_sort(struct STraverseParams *tp)
{
    const struct SMapDS *cn = (const struct SMapDS *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_d(&v2x->kv, cn->x.k);
        sv_push_gen(&v2x->vv, sso1_get(&cn->v));
    }
    return 0;
}
static int aux_ip_sort(struct STraverseParams *tp)
{
    const struct SMapIP *cn = (const struct SMapIP *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_i64(&v2x->kv, cn->x.k);
        sv_push_gen(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_dp_sort(struct STraverseParams *tp)
{
    const struct SMapDP *cn = (const struct SMapDP *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_d(&v2x->kv, cn->x.k);
        sv_push_gen(&v2x->vv, cn->v);
    }
    return 0;
}
static int aux_ss_sort(struct STraverseParams *tp)
{
    const struct SMapSS *cn = (const struct SMapSS *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_gen(&v2x->kv, sso_get(&cn->s));
        sv_push_gen(&v2x->vv, sso_get_s2(&cn->s));
    }
    return 0;
}
static int aux_sp_sort(struct STraverseParams *tp)
{
    const struct SMapSP *cn = (const struct SMapSP *)get_node_r(tp->t, tp->c);
    if (cn) {
        struct SV2X *v2x = (struct SV2X *)tp->context;
        sv_push_gen(&v2x->kv, sso1_get(&cn->x.k));
        sv_push_gen(&v2x->vv, cn->v);
    }
    return 0;
}

    /* clang-format off */

static const struct SMapCtx sm_ctx[SM0_NumTypes] = {
    {SV_I32, SV_I32, aux_i32_sort, NULL, (srt_cmp)cmp_i}, /*SM0_I32*/
    {SV_U32, SV_U32, aux_u32_sort, NULL, (srt_cmp)cmp_u}, /*SM0_U32*/
    {SV_I64, SV_I64, aux_i_sort, NULL, (srt_cmp)cmp_I}, /*SM0_I*/
    {SV_U64, SV_U64, aux_u_sort, NULL, (srt_cmp)cmp_U}, /*SM0_U*/
    {SV_F, SV_F, aux_f_sort, NULL, (srt_cmp)cmp_F}, /*SM0_F*/
    {SV_D, SV_D, aux_d_sort, NULL, (srt_cmp)cmp_D}, /*SM0_D*/
    {SV_GEN, SV_GEN, aux_s_sort, aux_sx_delete, (srt_cmp)cmp_S}, /*SM0_S*/
    {SV_I32, SV_I32, aux_ii32_sort, NULL, (srt_cmp)cmp_i}, /*SM0_II32*/
    {SV_U32, SV_U32, aux_uu32_sort, NULL, (srt_cmp)cmp_u}, /*SM0_UU32*/
    {SV_I64, SV_I64, aux_ii_sort, NULL, (srt_cmp)cmp_I}, /*SM0_II*/
    {SV_F, SV_F, aux_ff_sort, NULL, (srt_cmp)cmp_F}, /*SM0_FF*/
    {SV_D, SV_D, aux_dd_sort, NULL, (srt_cmp)cmp_D}, /*SM0_DD*/
    {SV_GEN, SV_I64, aux_si_sort, aux_sx_delete, (srt_cmp)cmp_S}, /*SM0_SI*/
    {SV_GEN, SV_U64, aux_su_sort, aux_sx_delete, (srt_cmp)cmp_S}, /*SM0_SU*/
    {SV_GEN, SV_D, aux_sd_sort, aux_sx_delete, (srt_cmp)cmp_S}, /*SM0_SD*/
    {SV_I64, SV_GEN, aux_is_sort, aux_is_delete, (srt_cmp)cmp_I}, /*SM0_IS*/
    {SV_U64, SV_GEN, aux_us_sort, aux_us_delete, (srt_cmp)cmp_U}, /*SM0_US*/
    {SV_D, SV_GEN, aux_ds_sort, aux_ds_delete, (srt_cmp)cmp_D}, /*SM0_DS*/
    {SV_I64, SV_GEN, aux_ip_sort, NULL, (srt_cmp)cmp_I}, /*SM0_IP*/
    {SV_D, SV_GEN, aux_dp_sort, NULL, (srt_cmp)cmp_D}, /*SM0_DP*/
    {SV_GEN, SV_GEN, aux_ss_sort, aux_ss_delete, (srt_cmp)cmp_S}, /*SM0_SS*/
    {SV_GEN, SV_GEN, aux_sp_sort, aux_sx_delete, (srt_cmp)cmp_S}, /*SM0_SP*/
};

S_INLINE srt_cmp type2cmpf(enum eSM_Type0 t)
{
    return t < SM0_NumTypes ? sm_ctx[t].cmpf : NULL;
}

S_INLINE srt_bool sm_chk_t(const srt_map *m, int t)
{
    return m && m->d.sub_type == t ? S_TRUE : S_FALSE;
}

S_INLINE srt_bool sm_chk_i32x(const srt_map *m)
{
    return (0  || sm_chk_t(m, SM0_I32) || sm_chk_t(m, SM0_II32));
}
S_INLINE srt_bool sm_chk_u32x(const srt_map *m)
{
    return (0  || sm_chk_t(m, SM0_U32) || sm_chk_t(m, SM0_UU32));
}
S_INLINE srt_bool sm_chk_ix(const srt_map *m)
{
    return (0  || sm_chk_t(m, SM0_I) || sm_chk_t(m, SM0_II) || sm_chk_t(m, SM0_IS) || sm_chk_t(m, SM0_IP));
}
S_INLINE srt_bool sm_chk_ux(const srt_map *m)
{
    return (0  || sm_chk_t(m, SM0_U) || sm_chk_t(m, SM0_US));
}
S_INLINE srt_bool sm_chk_fx(const srt_map *m)
{
    return (0  || sm_chk_t(m, SM0_F) || sm_chk_t(m, SM0_FF));
}
S_INLINE srt_bool sm_chk_dx(const srt_map *m)
{
    return (0  || sm_chk_t(m, SM0_D) || sm_chk_t(m, SM0_DD) || sm_chk_t(m, SM0_DS) || sm_chk_t(m, SM0_DP));
}
S_INLINE srt_bool sm_chk_sx(const srt_map *m)
{
    return (0  || sm_chk_t(m, SM0_S) || sm_chk_t(m, SM0_SI) || sm_chk_t(m, SM0_SU) || sm_chk_t(m, SM0_SD) || sm_chk_t(m, SM0_SS) || sm_chk_t(m, SM0_SP));
}

size_t sm_itr_ii32(const srt_map *m,
             int32_t kmin, int32_t kmax,
             srt_map_it_ii32 f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_II32, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_ni_i((const struct SMapi *)cn, kmin);
            cmpmax = cmp_ni_i((const struct SMapi *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapi *)cn)->k,
                               ((const struct SMapii *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_ni_i((const struct SMapi *)cn, kmin);
            cmpmax = cmp_ni_i((const struct SMapi *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapi *)cn)->k,
                               ((const struct SMapii *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_uu32(const srt_map *m,
             uint32_t kmin, uint32_t kmax,
             srt_map_it_uu32 f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_UU32, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nu_u((const struct SMapu *)cn, kmin);
            cmpmax = cmp_nu_u((const struct SMapu *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapu *)cn)->k,
                               ((const struct SMapuu *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nu_u((const struct SMapu *)cn, kmin);
            cmpmax = cmp_nu_u((const struct SMapu *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapu *)cn)->k,
                               ((const struct SMapuu *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_ii(const srt_map *m,
             int64_t kmin, int64_t kmax,
             srt_map_it_ii f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_II, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nI_I((const struct SMapI *)cn, kmin);
            cmpmax = cmp_nI_I((const struct SMapI *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapI *)cn)->k,
                               ((const struct SMapII *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nI_I((const struct SMapI *)cn, kmin);
            cmpmax = cmp_nI_I((const struct SMapI *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapI *)cn)->k,
                               ((const struct SMapII *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_ff(const srt_map *m,
             float kmin, float kmax,
             srt_map_it_ff f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_FF, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nF_F((const struct SMapF *)cn, kmin);
            cmpmax = cmp_nF_F((const struct SMapF *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapF *)cn)->k,
                               ((const struct SMapFF *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nF_F((const struct SMapF *)cn, kmin);
            cmpmax = cmp_nF_F((const struct SMapF *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapF *)cn)->k,
                               ((const struct SMapFF *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_dd(const srt_map *m,
             double kmin, double kmax,
             srt_map_it_dd f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_DD, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nD_D((const struct SMapD *)cn, kmin);
            cmpmax = cmp_nD_D((const struct SMapD *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapD *)cn)->k,
                               ((const struct SMapDD *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nD_D((const struct SMapD *)cn, kmin);
            cmpmax = cmp_nD_D((const struct SMapD *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapD *)cn)->k,
                               ((const struct SMapDD *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_si(const srt_map *m,
             const srt_string* kmin, const srt_string* kmax,
             srt_map_it_si f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_SI, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(    sso_get((const srt_stringo *)
            &((const struct SMapS *)cn)->k)
,
                               ((const struct SMapSI *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(    sso_get((const srt_stringo *)
            &((const struct SMapS *)cn)->k)
,
                               ((const struct SMapSI *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_su(const srt_map *m,
             const srt_string* kmin, const srt_string* kmax,
             srt_map_it_su f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_SU, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(    sso_get((const srt_stringo *)
            &((const struct SMapS *)cn)->k)
,
                               ((const struct SMapSU *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(    sso_get((const srt_stringo *)
            &((const struct SMapS *)cn)->k)
,
                               ((const struct SMapSU *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_sd(const srt_map *m,
             const srt_string* kmin, const srt_string* kmax,
             srt_map_it_sd f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_SD, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(    sso_get((const srt_stringo *)
            &((const struct SMapS *)cn)->k)
,
                               ((const struct SMapSD *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(    sso_get((const srt_stringo *)
            &((const struct SMapS *)cn)->k)
,
                               ((const struct SMapSD *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_is(const srt_map *m,
             int64_t kmin, int64_t kmax,
             srt_map_it_is f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_IS, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nI_I((const struct SMapI *)cn, kmin);
            cmpmax = cmp_nI_I((const struct SMapI *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapI *)cn)->k,
                                   sso_get((const srt_stringo *)
            &((const struct SMapIS *)cn)->v)
, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nI_I((const struct SMapI *)cn, kmin);
            cmpmax = cmp_nI_I((const struct SMapI *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapI *)cn)->k,
                                   sso_get((const srt_stringo *)
            &((const struct SMapIS *)cn)->v)
, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_us(const srt_map *m,
             uint64_t kmin, uint64_t kmax,
             srt_map_it_us f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_US, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nU_U((const struct SMapU *)cn, kmin);
            cmpmax = cmp_nU_U((const struct SMapU *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapU *)cn)->k,
                                   sso_get((const srt_stringo *)
            &((const struct SMapUS *)cn)->v)
, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nU_U((const struct SMapU *)cn, kmin);
            cmpmax = cmp_nU_U((const struct SMapU *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapU *)cn)->k,
                                   sso_get((const srt_stringo *)
            &((const struct SMapUS *)cn)->v)
, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_ds(const srt_map *m,
             double kmin, double kmax,
             srt_map_it_ds f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_DS, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nD_D((const struct SMapD *)cn, kmin);
            cmpmax = cmp_nD_D((const struct SMapD *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapD *)cn)->k,
                                   sso_get((const srt_stringo *)
            &((const struct SMapDS *)cn)->v)
, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nD_D((const struct SMapD *)cn, kmin);
            cmpmax = cmp_nD_D((const struct SMapD *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapD *)cn)->k,
                                   sso_get((const srt_stringo *)
            &((const struct SMapDS *)cn)->v)
, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_ip(const srt_map *m,
             int64_t kmin, int64_t kmax,
             srt_map_it_ip f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_IP, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nI_I((const struct SMapI *)cn, kmin);
            cmpmax = cmp_nI_I((const struct SMapI *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapI *)cn)->k,
                               ((const struct SMapIP *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nI_I((const struct SMapI *)cn, kmin);
            cmpmax = cmp_nI_I((const struct SMapI *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapI *)cn)->k,
                               ((const struct SMapIP *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_dp(const srt_map *m,
             double kmin, double kmax,
             srt_map_it_dp f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_DP, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nD_D((const struct SMapD *)cn, kmin);
            cmpmax = cmp_nD_D((const struct SMapD *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(((const struct SMapD *)cn)->k,
                               ((const struct SMapDP *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nD_D((const struct SMapD *)cn, kmin);
            cmpmax = cmp_nD_D((const struct SMapD *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(((const struct SMapD *)cn)->k,
                               ((const struct SMapDP *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_ss(const srt_map *m,
             const srt_string* kmin, const srt_string* kmax,
             srt_map_it_ss f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_SS, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(    sso_get(&((const struct SMapSS *)cn)->s)
,
                                   sso_get_s2(&((const struct SMapSS *)cn)->s)
, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(    sso_get(&((const struct SMapSS *)cn)->s)
,
                                   sso_get_s2(&((const struct SMapSS *)cn)->s)
, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
size_t sm_itr_sp(const srt_map *m,
             const srt_string* kmin, const srt_string* kmax,
             srt_map_it_sp f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != SM_SP, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !f(    sso_get((const srt_stringo *)
            &((const struct SMapS *)cn)->k)
,
                               ((const struct SMapSP *)cn)->v, context))
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = cmp_nS_S((const struct SMapS *)cn, kmin);
            cmpmax = cmp_nS_S((const struct SMapS *)cn, kmax);
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !f(    sso_get((const srt_stringo *)
            &((const struct SMapS *)cn)->k)
,
                               ((const struct SMapSP *)cn)->v, context))
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}

/*
 * Allocation
 */

srt_map *sm_alloc_raw0(enum eSM_Type0 t, srt_bool ext_buf, void *buffer,
               size_t elem_size, size_t max_size)
{
    srt_map *m;
    RETURN_IF(!buffer || !max_size, NULL);
    m = (srt_map *)st_alloc_raw(type2cmpf(t), ext_buf, buffer, elem_size,
                    max_size);
    if (m)
        m->d.sub_type = (uint8_t)t;
    return m;
}

srt_map *sm_alloc0(enum eSM_Type0 t, size_t init_size)
{
    srt_map *m = (srt_map *)st_alloc(type2cmpf(t), sm_elem_size((int)t),
                     init_size);
    if (m)
        m->d.sub_type = (uint8_t)t;
    return m;
}

void sm_free_aux(srt_map **m, ...)
{
    va_list ap;
    srt_map **next;
    va_start(ap, m);
    next = m;
    while (!s_varg_tail_ptr_tag(next)) { /* last element tag */
        if (next) {
            sm_clear(*next); /* release associated dyn. memory */
            sd_free((srt_data **)next);
        }
        next = (srt_map **)va_arg(ap, srt_map **);
    }
    va_end(ap);
}

srt_map *sm_dup(const srt_map *src)
{
    srt_map *m = NULL;
    return sm_cpy(&m, src);
}

void sm_clear(srt_map *m)
{
    int t;
    srt_tree_callback delete_callback;
    if (!m || !m->d.size)
        return;
    t = m->d.sub_type;
    delete_callback = t < SM0_NumTypes ? sm_ctx[t].delete_callback : NULL;
    if (delete_callback) { /* deletion of dynamic memory elems */
        srt_tndx i = 0;
        for (; i < (srt_tndx)m->d.size; i++) {
            srt_tnode *n = st_enum(m, i);
            delete_callback(n);
        }
    }
    st_set_size((srt_tree *)m, 0);
}

/*
 * Copy
 */

srt_map *sm_cpy(srt_map **m, const srt_map *src)
{
    srt_tndx i;
    enum eSM_Type0 t;
    size_t ss, src_buf_size;
    RETURN_IF(!m || !src, NULL); /* BEHAVIOR */
    t = (enum eSM_Type0)src->d.sub_type;
    ss = sm_size(src);
    src_buf_size = src->d.elem_size * src->d.size;
    RETURN_IF(ss > ST_NDX_MAX, NULL); /* BEHAVIOR */
    if (*m) {
        sm_clear(*m);
        if (!sm_chk_t(*m, (int)t)) {
            /*
             * Case of changing map type, reusing allocated memory,
             * but changing container configuration.
             */
            size_t raw_size = (*m)->d.elem_size * (*m)->d.max_size,
                   new_max_size = raw_size / src->d.elem_size;
            (*m)->d.elem_size = src->d.elem_size;
            (*m)->d.max_size = new_max_size;
            (*m)->cmp_f = src->cmp_f;
            (*m)->d.sub_type = src->d.sub_type;
        }
        sm_reserve(m, ss);
    } else {
        *m = sm_alloc0(t, ss);
    }
    RETURN_IF(!*m, NULL); /* BEHAVIOR: allocation error */
    RETURN_IF(sm_max_size(*m) < ss, *m); /* BEHAVIOR: not enough space */
    /*
     * Bulk tree copy: tree structure can be copied as is, because of
     * of using indexes instead of pointers.
     */
    memcpy(sm_get_buffer(*m), sm_get_buffer_r(src), src_buf_size);
    sm_set_size(*m, ss);
    (*m)->root = src->root;
    /*
     * Copy elements using external dynamic memory (string data)
     */
    switch (t) {
    case SM0_IS:
        for (i = 0; i < ss; i++) {
            const struct SMapIS *ms =
                (const struct SMapIS *)st_enum_r(src, i);
            struct SMapIS *mt = (struct SMapIS *)st_enum(*m, i);
            sso1_set(&mt->v, sso1_get(&ms->v));
        }
        break;
    case SM0_DS:
        for (i = 0; i < ss; i++) {
            const struct SMapDS *ms =
                (const struct SMapDS *)st_enum_r(src, i);
            struct SMapDS *mt = (struct SMapDS *)st_enum(*m, i);
            sso1_set(&mt->v, sso1_get(&ms->v));
        }
        break;
    case SM0_S:
    case SM0_SI:
    case SM0_SD:
    case SM0_SP:
        for (i = 0; i < ss; i++) {
            const struct SMapS *ms =
                (const struct SMapS *)st_enum_r(src, i);
            struct SMapS *mt = (struct SMapS *)st_enum(*m, i);
            sso1_set(&mt->k, sso1_get(&ms->k));
        }
        break;
    case SM0_SS:
        for (i = 0; i < ss; i++) {
            const struct SMapSS *ms =
                (const struct SMapSS *)st_enum_r(src, i);
            struct SMapSS *mt = (struct SMapSS *)st_enum(*m, i);
            sso_set(&mt->s, sso_get(&ms->s), sso_get_s2(&ms->s));
        }
        break;
    default:
        /* no additional action required */
        break;
    }
    return *m;
}

/*
 * Random access
 */

int32_t sm_at_ii32(const srt_map *m, int32_t k)
{
    struct SMapii n;
    const struct SMapii *nr;
    RETURN_IF(!sm_chk_t(m, SM_II32), 0);
    n.x.k = k;
    nr = (const struct SMapii *)st_locate(m, (const srt_tnode *)&n);
    return nr ? nr->v : 0;
}
uint32_t sm_at_uu32(const srt_map *m, uint32_t k)
{
    struct SMapuu n;
    const struct SMapuu *nr;
    RETURN_IF(!sm_chk_t(m, SM_UU32), 0);
    n.x.k = k;
    nr = (const struct SMapuu *)st_locate(m, (const srt_tnode *)&n);
    return nr ? nr->v : 0;
}
int64_t sm_at_ii(const srt_map *m, int64_t k)
{
    struct SMapII n;
    const struct SMapII *nr;
    RETURN_IF(!sm_chk_t(m, SM_II), 0);
    n.x.k = k;
    nr = (const struct SMapII *)st_locate(m, (const srt_tnode *)&n);
    return nr ? nr->v : 0;
}
float sm_at_ff(const srt_map *m, float k)
{
    struct SMapFF n;
    const struct SMapFF *nr;
    RETURN_IF(!sm_chk_t(m, SM_FF), 0);
    n.x.k = k;
    nr = (const struct SMapFF *)st_locate(m, (const srt_tnode *)&n);
    return nr ? nr->v : 0;
}
double sm_at_dd(const srt_map *m, double k)
{
    struct SMapDD n;
    const struct SMapDD *nr;
    RETURN_IF(!sm_chk_t(m, SM_DD), 0);
    n.x.k = k;
    nr = (const struct SMapDD *)st_locate(m, (const srt_tnode *)&n);
    return nr ? nr->v : 0;
}
int64_t sm_at_si(const srt_map *m, const srt_string* k)
{
    struct SMapSI n;
    const struct SMapSI *nr;
    RETURN_IF(!sm_chk_t(m, SM_SI), 0);
    sso1_setref(&n.x.k, k);
    nr = (const struct SMapSI *)st_locate(m, &n.x.n);
    return nr ? nr->v : 0;
}
uint64_t sm_at_su(const srt_map *m, const srt_string* k)
{
    struct SMapSU n;
    const struct SMapSU *nr;
    RETURN_IF(!sm_chk_t(m, SM_SU), 0);
    sso1_setref(&n.x.k, k);
    nr = (const struct SMapSU *)st_locate(m, &n.x.n);
    return nr ? nr->v : 0;
}
double sm_at_sd(const srt_map *m, const srt_string* k)
{
    struct SMapSD n;
    const struct SMapSD *nr;
    RETURN_IF(!sm_chk_t(m, SM_SD), 0);
    sso1_setref(&n.x.k, k);
    nr = (const struct SMapSD *)st_locate(m, &n.x.n);
    return nr ? nr->v : 0;
}
const srt_string* sm_at_is(const srt_map *m, int64_t k)
{
    struct SMapIS n;
    const struct SMapIS *nr;
    RETURN_IF(!sm_chk_t(m, SM_IS), ss_void);
    n.x.k = k;
    nr = (const struct SMapIS *)st_locate(m, (const srt_tnode *)&n);
    return nr ? sso1_get(&nr->v) : ss_void;
}
const srt_string* sm_at_us(const srt_map *m, uint64_t k)
{
    struct SMapUS n;
    const struct SMapUS *nr;
    RETURN_IF(!sm_chk_t(m, SM_US), ss_void);
    n.x.k = k;
    nr = (const struct SMapUS *)st_locate(m, (const srt_tnode *)&n);
    return nr ? sso1_get(&nr->v) : ss_void;
}
const srt_string* sm_at_ds(const srt_map *m, double k)
{
    struct SMapDS n;
    const struct SMapDS *nr;
    RETURN_IF(!sm_chk_t(m, SM_DS), ss_void);
    n.x.k = k;
    nr = (const struct SMapDS *)st_locate(m, (const srt_tnode *)&n);
    return nr ? sso1_get(&nr->v) : ss_void;
}
const void* sm_at_ip(const srt_map *m, int64_t k)
{
    struct SMapIP n;
    const struct SMapIP *nr;
    RETURN_IF(!sm_chk_t(m, SM_IP), NULL);
    n.x.k = k;
    nr = (const struct SMapIP *)st_locate(m, (const srt_tnode *)&n);
    return nr ? nr->v : NULL;
}
const void* sm_at_dp(const srt_map *m, double k)
{
    struct SMapDP n;
    const struct SMapDP *nr;
    RETURN_IF(!sm_chk_t(m, SM_DP), NULL);
    n.x.k = k;
    nr = (const struct SMapDP *)st_locate(m, (const srt_tnode *)&n);
    return nr ? nr->v : NULL;
}
const srt_string* sm_at_ss(const srt_map *m, const srt_string* k)
{
    struct SMapSS n;
    const struct SMapSS *nr;
    RETURN_IF(!sm_chk_t(m, SM_SS), ss_void);
    sso_setref(&n.s, k, NULL);
    nr = (const struct SMapSS *)st_locate(m, &n.n);
    return nr ? sso_get_s2(&nr->s) : ss_void;
}
const void* sm_at_sp(const srt_map *m, const srt_string* k)
{
    struct SMapSP n;
    const struct SMapSP *nr;
    RETURN_IF(!sm_chk_t(m, SM_SP), NULL);
    sso1_setref(&n.x.k, k);
    nr = (const struct SMapSP *)st_locate(m, &n.x.n);
    return nr ? nr->v : NULL;
}

/*
 * Existence check
 */

size_t sm_count_i32(const srt_map *m, int32_t k)
{
    struct SMapi n;
    RETURN_IF(!(sm_chk_i32x(m)), S_FALSE);
    RETURN_IF(sm_empty(m), S_FALSE); // if empty, do not even try
    n.k = k;
    return st_locate(m, (const srt_tnode *)&n) ? 1 : 0;
}
size_t sm_count_u32(const srt_map *m, uint32_t k)
{
    struct SMapu n;
    RETURN_IF(!(sm_chk_u32x(m)), S_FALSE);
    RETURN_IF(sm_empty(m), S_FALSE); // if empty, do not even try
    n.k = k;
    return st_locate(m, (const srt_tnode *)&n) ? 1 : 0;
}
size_t sm_count_i(const srt_map *m, int64_t k)
{
    struct SMapI n;
    RETURN_IF(!(sm_chk_ix(m)), S_FALSE);
    RETURN_IF(sm_empty(m), S_FALSE); // if empty, do not even try
    n.k = k;
    return st_locate(m, (const srt_tnode *)&n) ? 1 : 0;
}
size_t sm_count_u(const srt_map *m, uint64_t k)
{
    struct SMapU n;
    RETURN_IF(!(sm_chk_ux(m)), S_FALSE);
    RETURN_IF(sm_empty(m), S_FALSE); // if empty, do not even try
    n.k = k;
    return st_locate(m, (const srt_tnode *)&n) ? 1 : 0;
}
size_t sm_count_f(const srt_map *m, float k)
{
    struct SMapF n;
    RETURN_IF(!(sm_chk_fx(m)), S_FALSE);
    RETURN_IF(sm_empty(m), S_FALSE); // if empty, do not even try
    n.k = k;
    return st_locate(m, (const srt_tnode *)&n) ? 1 : 0;
}
size_t sm_count_d(const srt_map *m, double k)
{
    struct SMapD n;
    RETURN_IF(!(sm_chk_dx(m)), S_FALSE);
    RETURN_IF(sm_empty(m), S_FALSE); // if empty, do not even try
    n.k = k;
    return st_locate(m, (const srt_tnode *)&n) ? 1 : 0;
}
size_t sm_count_s(const srt_map *m, const srt_string* k)
{
    struct SMapS n;
    RETURN_IF(!(sm_chk_sx(m)), S_FALSE);
    RETURN_IF(sm_empty(m), S_FALSE); // if empty, do not even try
    sso1_setref(&n.k, k);
    return st_locate(m, (const srt_tnode *)&n) ? 1 : 0;
}

/*
 * Insert
 */

S_INLINE srt_bool sm_insert_ii32_aux(srt_map **m, int32_t k, int32_t v,
                                        srt_tree_rewrite rw_f)
{
    struct SMapii n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_II32), S_FALSE);
    n.x.k = k;
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n, rw_f);
}

srt_bool sm_insert_ii32(srt_map **m, int32_t k, int32_t v)
{
    return sm_insert_ii32_aux(m, k, v, NULL);
}

srt_bool sm_inc_ii32(srt_map **m, int32_t k, int32_t v)
{
    return sm_insert_ii32_aux(m, k, v, rw_inc_SM_II32);
}
S_INLINE srt_bool sm_insert_uu32_aux(srt_map **m, uint32_t k, uint32_t v,
                                        srt_tree_rewrite rw_f)
{
    struct SMapuu n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_UU32), S_FALSE);
    n.x.k = k;
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n, rw_f);
}

srt_bool sm_insert_uu32(srt_map **m, uint32_t k, uint32_t v)
{
    return sm_insert_uu32_aux(m, k, v, NULL);
}

srt_bool sm_inc_uu32(srt_map **m, uint32_t k, uint32_t v)
{
    return sm_insert_uu32_aux(m, k, v, rw_inc_SM_UU32);
}
S_INLINE srt_bool sm_insert_ii_aux(srt_map **m, int64_t k, int64_t v,
                                        srt_tree_rewrite rw_f)
{
    struct SMapII n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_II), S_FALSE);
    n.x.k = k;
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n, rw_f);
}

srt_bool sm_insert_ii(srt_map **m, int64_t k, int64_t v)
{
    return sm_insert_ii_aux(m, k, v, NULL);
}

srt_bool sm_inc_ii(srt_map **m, int64_t k, int64_t v)
{
    return sm_insert_ii_aux(m, k, v, rw_inc_SM_II);
}
S_INLINE srt_bool sm_insert_ff_aux(srt_map **m, float k, float v,
                                        srt_tree_rewrite rw_f)
{
    struct SMapFF n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_FF), S_FALSE);
    n.x.k = k;
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n, rw_f);
}

srt_bool sm_insert_ff(srt_map **m, float k, float v)
{
    return sm_insert_ff_aux(m, k, v, NULL);
}

srt_bool sm_inc_ff(srt_map **m, float k, float v)
{
    return sm_insert_ff_aux(m, k, v, rw_inc_SM_FF);
}
S_INLINE srt_bool sm_insert_dd_aux(srt_map **m, double k, double v,
                                        srt_tree_rewrite rw_f)
{
    struct SMapDD n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_DD), S_FALSE);
    n.x.k = k;
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n, rw_f);
}

srt_bool sm_insert_dd(srt_map **m, double k, double v)
{
    return sm_insert_dd_aux(m, k, v, NULL);
}

srt_bool sm_inc_dd(srt_map **m, double k, double v)
{
    return sm_insert_dd_aux(m, k, v, rw_inc_SM_DD);
}
S_INLINE srt_bool sm_insert_si_aux(srt_map **m, const srt_string* k, int64_t v,
                                        srt_tree_rewrite rw_f)
{
    struct SMapSI n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_SI), S_FALSE);
    sso1_setref(&n.x.k, k);
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n, rw_f);
}

srt_bool sm_insert_si(srt_map **m, const srt_string* k, int64_t v)
{
    return sm_insert_si_aux(m, k, v, rw_add_SM_SI);
}

srt_bool sm_inc_si(srt_map **m, const srt_string* k, int64_t v)
{
    return sm_insert_si_aux(m, k, v, rw_inc_SM_SI);
}
S_INLINE srt_bool sm_insert_su_aux(srt_map **m, const srt_string* k, uint64_t v,
                                        srt_tree_rewrite rw_f)
{
    struct SMapSU n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_SU), S_FALSE);
    sso1_setref(&n.x.k, k);
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n, rw_f);
}

srt_bool sm_insert_su(srt_map **m, const srt_string* k, uint64_t v)
{
    return sm_insert_su_aux(m, k, v, rw_add_SM_SU);
}

srt_bool sm_inc_su(srt_map **m, const srt_string* k, uint64_t v)
{
    return sm_insert_su_aux(m, k, v, rw_inc_SM_SU);
}
S_INLINE srt_bool sm_insert_sd_aux(srt_map **m, const srt_string* k, double v,
                                        srt_tree_rewrite rw_f)
{
    struct SMapSD n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_SD), S_FALSE);
    sso1_setref(&n.x.k, k);
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n, rw_f);
}

srt_bool sm_insert_sd(srt_map **m, const srt_string* k, double v)
{
    return sm_insert_sd_aux(m, k, v, rw_add_SM_SD);
}

srt_bool sm_inc_sd(srt_map **m, const srt_string* k, double v)
{
    return sm_insert_sd_aux(m, k, v, rw_inc_SM_SD);
}

srt_bool sm_insert_is(srt_map **m, int64_t k, const srt_string *v)
{
    struct SMapIS n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_IS), S_FALSE);
    n.x.k = k;
    sso1_setref(&n.v, v);
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n,
                        rw_add_SM_IS);
}
srt_bool sm_insert_us(srt_map **m, uint64_t k, const srt_string *v)
{
    struct SMapUS n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_US), S_FALSE);
    n.x.k = k;
    sso1_setref(&n.v, v);
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n,
                        rw_add_SM_US);
}
srt_bool sm_insert_ds(srt_map **m, double k, const srt_string *v)
{
    struct SMapDS n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_DS), S_FALSE);
    n.x.k = k;
    sso1_setref(&n.v, v);
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n,
                        rw_add_SM_DS);
}

srt_bool sm_insert_ip(srt_map **m, int64_t k, const void *v)
{
    struct SMapIP n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_IP), S_FALSE);
    n.x.k = k;
    n.v = v;
    return st_insert((srt_tree **)m, (const srt_tnode *)&n);
}
srt_bool sm_insert_dp(srt_map **m, double k, const void *v)
{
    struct SMapDP n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_DP), S_FALSE);
    n.x.k = k;
    n.v = v;
    return st_insert((srt_tree **)m, (const srt_tnode *)&n);
}

srt_bool sm_insert_ss(srt_map **m, const srt_string *k, const srt_string *v)
{
    struct SMapSS n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_SS), S_FALSE);
    sso_setref(&n.s, k, v);
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n,
                rw_add_SM_SS);
}

srt_bool sm_insert_sp(srt_map **m, const srt_string *k, const void *v)
{
    struct SMapSP n;
    RETURN_IF(!m || !sm_chk_t(*m, SM0_SP), S_FALSE);
    sso1_setref(&n.x.k, k);
    n.v = v;
    return st_insert_rw((srt_tree **)m, (const srt_tnode *)&n,
                rw_add_SM_SP);
}

/*
 * Delete
 */

srt_bool sm_delete_i32(srt_map *m, int32_t k)
{
    srt_tree_callback callback = NULL;
    struct SMapi n;
    n.k = k;
    RETURN_IF(!sm_chk_i32x(m), S_FALSE);
    return st_delete(m, (const srt_tnode *)&n, callback);
}
srt_bool sm_delete_u32(srt_map *m, uint32_t k)
{
    srt_tree_callback callback = NULL;
    struct SMapu n;
    n.k = k;
    RETURN_IF(!sm_chk_u32x(m), S_FALSE);
    return st_delete(m, (const srt_tnode *)&n, callback);
}
srt_bool sm_delete_i(srt_map *m, int64_t k)
{
    srt_tree_callback callback = NULL;
    struct SMapI n;
    n.k = k;
    RETURN_IF(!sm_chk_ix(m), S_FALSE);
callback = sm_chk_t(m, SM0_IS) ? aux_is_delete : NULL;
    return st_delete(m, (const srt_tnode *)&n, callback);
}
srt_bool sm_delete_u(srt_map *m, uint64_t k)
{
    srt_tree_callback callback = NULL;
    struct SMapU n;
    n.k = k;
    RETURN_IF(!sm_chk_ux(m), S_FALSE);
    return st_delete(m, (const srt_tnode *)&n, callback);
}
srt_bool sm_delete_f(srt_map *m, float k)
{
    srt_tree_callback callback = NULL;
    struct SMapF n;
    n.k = k;
    RETURN_IF(!sm_chk_fx(m), S_FALSE);
    return st_delete(m, (const srt_tnode *)&n, callback);
}
srt_bool sm_delete_d(srt_map *m, double k)
{
    srt_tree_callback callback = NULL;
    struct SMapD n;
    n.k = k;
    RETURN_IF(!sm_chk_dx(m), S_FALSE);
callback = sm_chk_t(m, SM0_DS) ? aux_ds_delete : NULL;
    return st_delete(m, (const srt_tnode *)&n, callback);
}
srt_bool sm_delete_s(srt_map *m, const srt_string* k)
{
    srt_tree_callback callback = NULL;
    struct SMapS n;
    sso1_setref(&n.k, k);
    RETURN_IF(!m, S_FALSE);
if (sm_chk_t(m, SM0_SS)) callback = aux_ss_delete;
else if (sm_chk_sx(m)) callback = aux_sx_delete;
else return S_FALSE;
    return st_delete(m, (const srt_tnode *)&n, callback);
}

/*
 * Enumeration / export data
 */

ssize_t sm_sort_to_vectors(const srt_map *m, srt_vector **kv, srt_vector **vv)
{
    ssize_t r;
    struct SV2X v2x;
    uint8_t t;
    enum eSV_Type kt, vt;
    st_traverse traverse_f = NULL;
    RETURN_IF(!m || !kv || !vv || m->d.sub_type >= SM0_NumTypes, 0);
    v2x.kv = *kv;
    v2x.vv = *vv;
    t = m->d.sub_type;
    kt = sm_ctx[t].sort_kt;
    vt = sm_ctx[t].sort_vt;
    traverse_f = sm_ctx[t].sort_tr;
    if (v2x.kv) {
        if (v2x.kv->d.sub_type != kt)
            sv_free(&v2x.kv);
        else
            sv_reserve(&v2x.kv, m->d.size);
    }
    if (v2x.vv) {
        if (v2x.vv->d.sub_type != vt)
            sv_free(&v2x.vv);
        else
            sv_reserve(&v2x.vv, m->d.size);
    }
    if (!v2x.kv)
        v2x.kv = sv_alloc_t(kt, m->d.size);
    if (!v2x.vv)
        v2x.vv = sv_alloc_t(vt, m->d.size);
    r = st_traverse_inorder((const srt_tree *)m, traverse_f, (void *)&v2x);
    *kv = v2x.kv;
    *vv = v2x.vv;
    return r;
}
