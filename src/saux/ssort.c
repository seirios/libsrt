/*
 * ssort.c
 *
 * Sorting algorithms
 *
 * Copyright (c) 2015-2019 F. Aragon. All rights reserved.
 * Released under the BSD 3-Clause License (see the doc/LICENSE)
 */

#include "ssort.h"

#ifndef S_MINIMAL

/* clang-format off */

S_INLINE void s_count_sort_i8(int8_t *b, size_t elems)
{
    size_t i, j;
    size_t cnt[256];
    memset(cnt, 0, sizeof(cnt));
    for (i = 0; i < elems; i++)
        cnt[b[i] + (1<<7)]++;
    for (i = j = 0; j < 256 && i < elems; j++)
        if (cnt[j]) {
            memset(b + i, (int)(j - (1<<7)), cnt[j]);
            i += cnt[j];
        }
}
S_INLINE void s_count_sort_u8(uint8_t *b, size_t elems)
{
    size_t i, j;
    size_t cnt[256];
    memset(cnt, 0, sizeof(cnt));
    for (i = 0; i < elems; i++)
        cnt[b[i] + (0)]++;
    for (i = j = 0; j < 256 && i < elems; j++)
        if (cnt[j]) {
            memset(b + i, (int)(j - (0)), cnt[j]);
            i += cnt[j];
        }
}
S_INLINE void s_count_sort_i8_small(int8_t *b, size_t elems)
{
    size_t i, j;
    uint8_t cnt[256];
    memset(cnt, 0, sizeof(cnt));
    for (i = 0; i < elems; i++)
        cnt[b[i] + (1<<7)]++;
    for (i = j = 0; j < 256 && i < elems; j++)
        if (cnt[j]) {
            memset(b + i, (int)(j - (1<<7)), cnt[j]);
            i += cnt[j];
        }
}
S_INLINE void s_count_sort_u8_small(uint8_t *b, size_t elems)
{
    size_t i, j;
    uint8_t cnt[256];
    memset(cnt, 0, sizeof(cnt));
    for (i = 0; i < elems; i++)
        cnt[b[i] + (0)]++;
    for (i = j = 0; j < 256 && i < elems; j++)
        if (cnt[j]) {
            memset(b + i, (int)(j - (0)), cnt[j]);
            i += cnt[j];
        }
}
S_INLINE void s_swap_i16(int16_t *b, size_t i, size_t j)
{
    int16_t tmp = b[i];
    b[i] = b[j];
    b[j] = tmp;
}
S_INLINE void s_sort2_i16(int16_t *b)
{
    if (b[0] > b[1])
        s_swap_i16(b, 0, 1);
}
S_INLINE void s_sort3_i16(int16_t *b)
{
    if (b[0] > b[2])
        s_swap_i16(b, 0, 2);
    s_sort2_i16(b);
}
S_INLINE void s_sort4_i16(int16_t *b)
{
    s_sort2_i16(b);
    s_sort2_i16(b + 2);
    if (b[2] < b[0])
        s_swap_i16(b, 0, 2);
    s_sort2_i16(b + 1);
    if (b[2] < b[1])
        s_swap_i16(b, 1, 2);
    s_sort2_i16(b + 2);
}
S_INLINE void s_swap_u16(uint16_t *b, size_t i, size_t j)
{
    uint16_t tmp = b[i];
    b[i] = b[j];
    b[j] = tmp;
}
S_INLINE void s_sort2_u16(uint16_t *b)
{
    if (b[0] > b[1])
        s_swap_u16(b, 0, 1);
}
S_INLINE void s_sort3_u16(uint16_t *b)
{
    if (b[0] > b[2])
        s_swap_u16(b, 0, 2);
    s_sort2_u16(b);
}
S_INLINE void s_sort4_u16(uint16_t *b)
{
    s_sort2_u16(b);
    s_sort2_u16(b + 2);
    if (b[2] < b[0])
        s_swap_u16(b, 0, 2);
    s_sort2_u16(b + 1);
    if (b[2] < b[1])
        s_swap_u16(b, 1, 2);
    s_sort2_u16(b + 2);
}
S_INLINE void s_swap_i32(int32_t *b, size_t i, size_t j)
{
    int32_t tmp = b[i];
    b[i] = b[j];
    b[j] = tmp;
}
S_INLINE void s_sort2_i32(int32_t *b)
{
    if (b[0] > b[1])
        s_swap_i32(b, 0, 1);
}
S_INLINE void s_sort3_i32(int32_t *b)
{
    if (b[0] > b[2])
        s_swap_i32(b, 0, 2);
    s_sort2_i32(b);
}
S_INLINE void s_sort4_i32(int32_t *b)
{
    s_sort2_i32(b);
    s_sort2_i32(b + 2);
    if (b[2] < b[0])
        s_swap_i32(b, 0, 2);
    s_sort2_i32(b + 1);
    if (b[2] < b[1])
        s_swap_i32(b, 1, 2);
    s_sort2_i32(b + 2);
}
S_INLINE void s_swap_u32(uint32_t *b, size_t i, size_t j)
{
    uint32_t tmp = b[i];
    b[i] = b[j];
    b[j] = tmp;
}
S_INLINE void s_sort2_u32(uint32_t *b)
{
    if (b[0] > b[1])
        s_swap_u32(b, 0, 1);
}
S_INLINE void s_sort3_u32(uint32_t *b)
{
    if (b[0] > b[2])
        s_swap_u32(b, 0, 2);
    s_sort2_u32(b);
}
S_INLINE void s_sort4_u32(uint32_t *b)
{
    s_sort2_u32(b);
    s_sort2_u32(b + 2);
    if (b[2] < b[0])
        s_swap_u32(b, 0, 2);
    s_sort2_u32(b + 1);
    if (b[2] < b[1])
        s_swap_u32(b, 1, 2);
    s_sort2_u32(b + 2);
}
S_INLINE void s_swap_i64(int64_t *b, size_t i, size_t j)
{
    int64_t tmp = b[i];
    b[i] = b[j];
    b[j] = tmp;
}
S_INLINE void s_sort2_i64(int64_t *b)
{
    if (b[0] > b[1])
        s_swap_i64(b, 0, 1);
}
S_INLINE void s_sort3_i64(int64_t *b)
{
    if (b[0] > b[2])
        s_swap_i64(b, 0, 2);
    s_sort2_i64(b);
}
S_INLINE void s_sort4_i64(int64_t *b)
{
    s_sort2_i64(b);
    s_sort2_i64(b + 2);
    if (b[2] < b[0])
        s_swap_i64(b, 0, 2);
    s_sort2_i64(b + 1);
    if (b[2] < b[1])
        s_swap_i64(b, 1, 2);
    s_sort2_i64(b + 2);
}
S_INLINE void s_swap_u64(uint64_t *b, size_t i, size_t j)
{
    uint64_t tmp = b[i];
    b[i] = b[j];
    b[j] = tmp;
}
S_INLINE void s_sort2_u64(uint64_t *b)
{
    if (b[0] > b[1])
        s_swap_u64(b, 0, 1);
}
S_INLINE void s_sort3_u64(uint64_t *b)
{
    if (b[0] > b[2])
        s_swap_u64(b, 0, 2);
    s_sort2_u64(b);
}
S_INLINE void s_sort4_u64(uint64_t *b)
{
    s_sort2_u64(b);
    s_sort2_u64(b + 2);
    if (b[2] < b[0])
        s_swap_u64(b, 0, 2);
    s_sort2_u64(b + 1);
    if (b[2] < b[1])
        s_swap_u64(b, 1, 2);
    s_sort2_u64(b + 2);
}
static void s_msd_radix_sort_i16_aux(uint16_t acc, uint16_t msd_bit,
                      int16_t *b, size_t elems)
{
    uint16_t aux;
    size_t i = 0, j = elems - 1;
    for (; i < elems; i++) {
        aux = (uint16_t)b[i] + (1<<15);
        if ((aux & msd_bit) != 0) {
            for (; (((uint16_t)b[j] + (1<<15)) & msd_bit) != 0
                   && i < j;
                 j--)
                ;
            if (j <= i)
                break;
            s_swap_i16(b, i, j);
            j--;
        }
    }
    acc &= ~msd_bit;
    if (acc) {
        size_t elems0 = i, elems1 = elems - elems0;
        if (elems0 > 4 || elems1 > 4)
            msd_bit = s_msb16(acc);
        if (elems0 > 1) {
            if (elems0 > 4)
                s_msd_radix_sort_i16_aux(acc, msd_bit, b, elems0);
            else if (elems0 == 4)
                s_sort4_i16(b);
            else if (elems0 == 3)
                s_sort3_i16(b);
            else
                s_sort2_i16(b);
        }
        if (elems1 > 1) {
            if (elems1 > 4)
                s_msd_radix_sort_i16_aux(acc, msd_bit, b + i, elems1);
            else if (elems1 == 4)
                s_sort4_i16(b + i);
            else if (elems1 == 3)
                s_sort3_i16(b + i);
            else
                s_sort2_i16(b + i);
        }
    }
}

static void s_msd_radix_sort_i16(int16_t *b, size_t elems)
{
    size_t i;
    uint16_t acc = 0;
    for (i = 1; i < elems; i++)
        acc |= (((uint16_t)b[i - 1] + (1<<15)) ^
                ((uint16_t)b[i] + (1<<15)));
    if (acc)
        s_msd_radix_sort_i16_aux(acc, s_msb16(acc), b, elems);
}
static void s_msd_radix_sort_i32_aux(uint32_t acc, uint32_t msd_bit,
                      int32_t *b, size_t elems)
{
    uint32_t aux;
    size_t i = 0, j = elems - 1;
    for (; i < elems; i++) {
        aux = (uint32_t)b[i] + (1UL<<31);
        if ((aux & msd_bit) != 0) {
            for (; (((uint32_t)b[j] + (1UL<<31)) & msd_bit) != 0
                   && i < j;
                 j--)
                ;
            if (j <= i)
                break;
            s_swap_i32(b, i, j);
            j--;
        }
    }
    acc &= ~msd_bit;
    if (acc) {
        size_t elems0 = i, elems1 = elems - elems0;
        if (elems0 > 4 || elems1 > 4)
            msd_bit = s_msb32(acc);
        if (elems0 > 1) {
            if (elems0 > 4)
                s_msd_radix_sort_i32_aux(acc, msd_bit, b, elems0);
            else if (elems0 == 4)
                s_sort4_i32(b);
            else if (elems0 == 3)
                s_sort3_i32(b);
            else
                s_sort2_i32(b);
        }
        if (elems1 > 1) {
            if (elems1 > 4)
                s_msd_radix_sort_i32_aux(acc, msd_bit, b + i, elems1);
            else if (elems1 == 4)
                s_sort4_i32(b + i);
            else if (elems1 == 3)
                s_sort3_i32(b + i);
            else
                s_sort2_i32(b + i);
        }
    }
}

static void s_msd_radix_sort_i32(int32_t *b, size_t elems)
{
    size_t i;
    uint32_t acc = 0;
    for (i = 1; i < elems; i++)
        acc |= (((uint32_t)b[i - 1] + (1UL<<31)) ^
                ((uint32_t)b[i] + (1UL<<31)));
    if (acc)
        s_msd_radix_sort_i32_aux(acc, s_msb32(acc), b, elems);
}
static void s_msd_radix_sort_i64_aux(uint64_t acc, uint64_t msd_bit,
                      int64_t *b, size_t elems)
{
    uint64_t aux;
    size_t i = 0, j = elems - 1;
    for (; i < elems; i++) {
        aux = (uint64_t)b[i] + ((uint64_t)1<<63);
        if ((aux & msd_bit) != 0) {
            for (; (((uint64_t)b[j] + ((uint64_t)1<<63)) & msd_bit) != 0
                   && i < j;
                 j--)
                ;
            if (j <= i)
                break;
            s_swap_i64(b, i, j);
            j--;
        }
    }
    acc &= ~msd_bit;
    if (acc) {
        size_t elems0 = i, elems1 = elems - elems0;
        if (elems0 > 4 || elems1 > 4)
            msd_bit = s_msb64(acc);
        if (elems0 > 1) {
            if (elems0 > 4)
                s_msd_radix_sort_i64_aux(acc, msd_bit, b, elems0);
            else if (elems0 == 4)
                s_sort4_i64(b);
            else if (elems0 == 3)
                s_sort3_i64(b);
            else
                s_sort2_i64(b);
        }
        if (elems1 > 1) {
            if (elems1 > 4)
                s_msd_radix_sort_i64_aux(acc, msd_bit, b + i, elems1);
            else if (elems1 == 4)
                s_sort4_i64(b + i);
            else if (elems1 == 3)
                s_sort3_i64(b + i);
            else
                s_sort2_i64(b + i);
        }
    }
}

static void s_msd_radix_sort_i64(int64_t *b, size_t elems)
{
    size_t i;
    uint64_t acc = 0;
    for (i = 1; i < elems; i++)
        acc |= (((uint64_t)b[i - 1] + ((uint64_t)1<<63)) ^
                ((uint64_t)b[i] + ((uint64_t)1<<63)));
    if (acc)
        s_msd_radix_sort_i64_aux(acc, s_msb64(acc), b, elems);
}
static void s_msd_radix_sort_u16_aux(uint16_t acc, uint16_t msd_bit,
                      uint16_t *b, size_t elems)
{
    uint16_t aux;
    size_t i = 0, j = elems - 1;
    for (; i < elems; i++) {
        aux = (uint16_t)b[i] + (0);
        if ((aux & msd_bit) != 0) {
            for (; (((uint16_t)b[j] + (0)) & msd_bit) != 0
                   && i < j;
                 j--)
                ;
            if (j <= i)
                break;
            s_swap_u16(b, i, j);
            j--;
        }
    }
    acc &= ~msd_bit;
    if (acc) {
        size_t elems0 = i, elems1 = elems - elems0;
        if (elems0 > 4 || elems1 > 4)
            msd_bit = s_msb16(acc);
        if (elems0 > 1) {
            if (elems0 > 4)
                s_msd_radix_sort_u16_aux(acc, msd_bit, b, elems0);
            else if (elems0 == 4)
                s_sort4_u16(b);
            else if (elems0 == 3)
                s_sort3_u16(b);
            else
                s_sort2_u16(b);
        }
        if (elems1 > 1) {
            if (elems1 > 4)
                s_msd_radix_sort_u16_aux(acc, msd_bit, b + i, elems1);
            else if (elems1 == 4)
                s_sort4_u16(b + i);
            else if (elems1 == 3)
                s_sort3_u16(b + i);
            else
                s_sort2_u16(b + i);
        }
    }
}

static void s_msd_radix_sort_u16(uint16_t *b, size_t elems)
{
    size_t i;
    uint16_t acc = 0;
    for (i = 1; i < elems; i++)
        acc |= (((uint16_t)b[i - 1] + (0)) ^
                ((uint16_t)b[i] + (0)));
    if (acc)
        s_msd_radix_sort_u16_aux(acc, s_msb16(acc), b, elems);
}
static void s_msd_radix_sort_u32_aux(uint32_t acc, uint32_t msd_bit,
                      uint32_t *b, size_t elems)
{
    uint32_t aux;
    size_t i = 0, j = elems - 1;
    for (; i < elems; i++) {
        aux = (uint32_t)b[i] + (0);
        if ((aux & msd_bit) != 0) {
            for (; (((uint32_t)b[j] + (0)) & msd_bit) != 0
                   && i < j;
                 j--)
                ;
            if (j <= i)
                break;
            s_swap_u32(b, i, j);
            j--;
        }
    }
    acc &= ~msd_bit;
    if (acc) {
        size_t elems0 = i, elems1 = elems - elems0;
        if (elems0 > 4 || elems1 > 4)
            msd_bit = s_msb32(acc);
        if (elems0 > 1) {
            if (elems0 > 4)
                s_msd_radix_sort_u32_aux(acc, msd_bit, b, elems0);
            else if (elems0 == 4)
                s_sort4_u32(b);
            else if (elems0 == 3)
                s_sort3_u32(b);
            else
                s_sort2_u32(b);
        }
        if (elems1 > 1) {
            if (elems1 > 4)
                s_msd_radix_sort_u32_aux(acc, msd_bit, b + i, elems1);
            else if (elems1 == 4)
                s_sort4_u32(b + i);
            else if (elems1 == 3)
                s_sort3_u32(b + i);
            else
                s_sort2_u32(b + i);
        }
    }
}

static void s_msd_radix_sort_u32(uint32_t *b, size_t elems)
{
    size_t i;
    uint32_t acc = 0;
    for (i = 1; i < elems; i++)
        acc |= (((uint32_t)b[i - 1] + (0)) ^
                ((uint32_t)b[i] + (0)));
    if (acc)
        s_msd_radix_sort_u32_aux(acc, s_msb32(acc), b, elems);
}
static void s_msd_radix_sort_u64_aux(uint64_t acc, uint64_t msd_bit,
                      uint64_t *b, size_t elems)
{
    uint64_t aux;
    size_t i = 0, j = elems - 1;
    for (; i < elems; i++) {
        aux = (uint64_t)b[i] + (0);
        if ((aux & msd_bit) != 0) {
            for (; (((uint64_t)b[j] + (0)) & msd_bit) != 0
                   && i < j;
                 j--)
                ;
            if (j <= i)
                break;
            s_swap_u64(b, i, j);
            j--;
        }
    }
    acc &= ~msd_bit;
    if (acc) {
        size_t elems0 = i, elems1 = elems - elems0;
        if (elems0 > 4 || elems1 > 4)
            msd_bit = s_msb64(acc);
        if (elems0 > 1) {
            if (elems0 > 4)
                s_msd_radix_sort_u64_aux(acc, msd_bit, b, elems0);
            else if (elems0 == 4)
                s_sort4_u64(b);
            else if (elems0 == 3)
                s_sort3_u64(b);
            else
                s_sort2_u64(b);
        }
        if (elems1 > 1) {
            if (elems1 > 4)
                s_msd_radix_sort_u64_aux(acc, msd_bit, b + i, elems1);
            else if (elems1 == 4)
                s_sort4_u64(b + i);
            else if (elems1 == 3)
                s_sort3_u64(b + i);
            else
                s_sort2_u64(b + i);
        }
    }
}

static void s_msd_radix_sort_u64(uint64_t *b, size_t elems)
{
    size_t i;
    uint64_t acc = 0;
    for (i = 1; i < elems; i++)
        acc |= (((uint64_t)b[i - 1] + (0)) ^
                ((uint64_t)b[i] + (0)));
    if (acc)
        s_msd_radix_sort_u64_aux(acc, s_msb64(acc), b, elems);
}

/*
 * Sort functions
 */

#define SSORT_CHECK(b, elems)                                              \
    if (!b || elems <= 1)                                                  \
    return

void ssort_i8(int8_t *b, size_t elems)
{
    SSORT_CHECK(b, elems);
    if (elems < 256)
        s_count_sort_i8_small(b, elems);
    else
        s_count_sort_i8(b, elems);
}

/* clang-format on */

void ssort_u8(uint8_t *b, size_t elems)
{
    SSORT_CHECK(b, elems);
    if (elems < 256)
        s_count_sort_u8_small(b, elems);
    else
        s_count_sort_u8(b, elems);
}

void ssort_i16(int16_t *b, size_t elems)
{
    SSORT_CHECK(b, elems);
    s_msd_radix_sort_i16(b, elems);
}

void ssort_u16(uint16_t *b, size_t elems)
{
    SSORT_CHECK(b, elems);
    s_msd_radix_sort_u16(b, elems);
}
void ssort_i32(int32_t *b, size_t elems)
{
    SSORT_CHECK(b, elems);
    s_msd_radix_sort_i32(b, elems);
}

void ssort_u32(uint32_t *b, size_t elems)
{
    SSORT_CHECK(b, elems);
    s_msd_radix_sort_u32(b, elems);
}
void ssort_i64(int64_t *b, size_t elems)
{
    SSORT_CHECK(b, elems);
    s_msd_radix_sort_i64(b, elems);
}

void ssort_u64(uint64_t *b, size_t elems)
{
    SSORT_CHECK(b, elems);
    s_msd_radix_sort_u64(b, elems);
}

#endif /* #ifndef S_MINIMAL */
