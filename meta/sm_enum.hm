%// Why are these different from regular :kget_smap and :vget_smap???

%snippet knum_vnum:kget_sm_enum = (x) %{ ${x}->k %}
%snippet knum_vnum:vget_sm_enum = (x) %{ ${x}->v %}

%snippet knum_vstr:kget_sm_enum = (x) %{ ${x}->k %}
%snippet knum_vstr:vget_sm_enum = (x) %{
    sso_get((const srt_stringo *)
            &${x}->v)
%}

%snippet knum_vptr:kget_sm_enum = (x) %{ ${x}->k %}
%snippet knum_vptr:vget_sm_enum = (x) %{ ${x}->v %}

%snippet kstr_vnum:kget_sm_enum = (x) %{
    sso_get((const srt_stringo *)
            &${x}->k)
%}
%snippet kstr_vnum:vget_sm_enum = (x) %{ ${x}->v %}

%snippet kstr_vptr:kget_sm_enum = (x) %{
    sso_get((const srt_stringo *)
            &${x}->k)
%}
%snippet kstr_vptr:vget_sm_enum = (x) %{ ${x}->v %}

%snippet kstr_vstr:kget_sm_enum = (x) %{
    sso_get(&((const struct SMapSS *)cn)->s)
%}
%snippet kstr_vstr:vget_sm_enum = (x) %{
    sso_get_s2(&((const struct SMapSS *)cn)->s)
%}

%snippet sm_enum_inorder = (fn, cb_t, map_t, key_t, tr_cmp_min, tr_cmp_max, tr_cb) %{
size_t ${fn}(const srt_map *m,
             ${key_t} kmin, ${key_t} kmax,
             ${cb_t} f, void *context)
{
    ssize_t level;
    size_t ts, nelems, rbt_max_depth;
    struct STreeScan *p;
    const srt_tnode *cn;
    int cmpmin, cmpmax;
    RETURN_IF(!m, 0);            /* null tree */
    RETURN_IF(m->d.sub_type != ${map_t}, 0); /* wrong type */
    ts = sm_size(m);
    RETURN_IF(!ts, S_FALSE); /* empty tree */
    level = 0;
    nelems = 0;
    rbt_max_depth = 2 * (slog2(ts) + 1);
    p = (struct STreeScan *)s_alloca(sizeof(struct STreeScan)
                     * (rbt_max_depth + 3));
    ASSERT_RETURN_IF(!p, 0); /* BEHAVIOR: stack error */
    p[0].p = ST_NIL;
    p[0].c = m->root;
    p[0].s = STS_ScanStart;
    while (level >= 0) {
        S_ASSERT(level <= (ssize_t)rbt_max_depth);
        switch (p[level].s) {
        case STS_ScanStart:
            cn = get_node_r(m, p[level].c);
            cmpmin = ${tr_cmp_min};
            cmpmax = ${tr_cmp_max};
            if (cn->x.l != ST_NIL && cmpmin > 0) {
                p[level].s = STS_ScanLeft;
                level++;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->x.l;
            } else {
                /* node with null left children */
                if (cmpmin >= 0 && cmpmax <= 0) {
                    if (f && !${tr_cb})
                        return nelems;
                    nelems++;
                }
                if (cn->r != ST_NIL && cmpmax < 0) {
                    p[level].s = STS_ScanRight;
                    level++;
                    cn = get_node_r(m, p[level - 1].c);
                    p[level].c = cn->r;
                } else {
                    p[level].s = STS_ScanDone;
                    level--;
                    continue;
                }
            }
            p[level].p = p[level - 1].c;
            p[level].s = STS_ScanStart;
            continue;
        case STS_ScanLeft:
            cn = get_node_r(m, p[level].c);
            cmpmin = ${tr_cmp_min};
            cmpmax = ${tr_cmp_max};
            if (cmpmin >= 0 && cmpmax <= 0) {
                if (f && !${tr_cb})
                    return nelems;
                nelems++;
            }
            if (cn->r != ST_NIL && cmpmax < 0) {
                p[level].s = STS_ScanRight;
                level++;
                p[level].p = p[level - 1].c;
                cn = get_node_r(m, p[level - 1].c);
                p[level].c = cn->r;
                p[level].s = STS_ScanStart;
            } else {
                p[level].s = STS_ScanDone;
                level--;
                continue;
            }
            continue;
        case STS_ScanRight:
        case STS_ScanDone:
            p[level].s = STS_ScanDone;
            level--;
            continue;
        }
    }
    return nelems;
}
%}
