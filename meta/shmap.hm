#ifndef SHMAP_H
#define SHMAP_H
#ifdef __cplusplus
extern "C" {
#endif

%include "maptypes.hm"

/*
 * shmap.h
 *
 * #SHORTDOC hash map handling (key-value storage)
 *
 * #DOC Map functions handle key-value storage, which is implemented as a
 * #DOC hash table (O(n), with O(1) amortized time complexity for
 * #DOC insert/read/delete)
 * #DOC
 * #DOC
 * #DOC Supported key/value modes (enum eSHM_Type):
 * #DOC
%map map_types:pair_num, map_types:pair_ptr %{
 * #DOC
 * #DOC SHM_$U{name}: ${doc_ktype} key, ${doc_vtype} value
%}
 * #DOC
 * #DOC
 * #DOC Callback types for the shm_itp_*() functions:
 * #DOC
%map map_types:pair_num, map_types:pair_ptr %{
 * #DOC
 * #DOC typedef srt_bool (*srt_hmap_it_${name})(${ktype} k, ${vtype} v, void *context);
%}
 *
 * Copyright (c) 2015-2020 F. Aragon. All rights reserved.
 * Released under the BSD 3-Clause License (see the doc/LICENSE)
 */

#include "saux/scommon.h"
#include "saux/sstringo.h"

/*
 * Structures and types
 */

enum eSHM_Type0 {
%map map_types:single, map_types:pair_num, map_types:pair_ptr %{
    SHM0_$U{name},
%}
    SHM0_NumTypes
};

enum eSHM_Type {
%map map_types:pair_num, map_types:pair_ptr %{
    SHM_$U{name} = SHM0_$U{name},
%}
};

%map map_types:knum %{
struct SHMap${stype} {
    ${ktype} k;
};
%}

struct SHMapS {
    srt_stringo1 k;
};

struct SHMapSS {
    srt_stringo kv;
};

%map map_types:knum_vnum %{
struct SHMap${stype} {
    struct SHMap${kstype} x;
    ${vtype} v;
};
%}

%map map_types:knum_vstr %{
struct SHMap${stype} {
    struct SHMap${kstype} x;
    srt_stringo1 v;
};
%}

%map map_types:knum_vptr, map_types:kstr_vptr %{
struct SHMap${stype} {
    struct SHMap${kstype} x;
    const void *v;
};
%}

%map map_types:kstr_vnum %{
struct SHMap${stype} {
    struct SHMap${kstype} x;
    ${vtype} v;
};
%}

typedef struct S_HMap srt_hmap;

typedef uint32_t shm_eloc_t_; /* element location offset */

struct SHMBucket {
    /*
     * Location where the bucket associated data is stored
     */
    shm_eloc_t_ loc;
    /*
     * Hash of the element (the bucket id would be the N highest bits)
     */
    uint32_t hash;
    /*
     * Bucket collision counter
     * 0: Zero elements associated to the bucket. This means that no
     *    element with the hash associated to the bucket has been inserted
     * >= 1: Number of elements associated to the bucket.
     */
    uint32_t cnt;
};

/*
 * srt_hmap memory layout:
 *
 * | SDataFull | struct fields | struct SHMBucket [N] | elements [M] |
 */

typedef srt_bool (*shm_eq_f)(const void *key, const void *node);
typedef void (*shm_del_f)(void *node);
typedef uint32_t (*shm_hash_f)(const void *node);
typedef const void *(*shm_n2key_f)(const void *node);

struct S_HMap {
    struct SDataFull d;
    uint32_t hbits; /* hash table bits */
    uint32_t hmask; /* hash table bitmask */
    size_t rh_threshold; /* (1 << hbits) * rh_threshold_pct) / 100 */
    size_t rh_threshold_pct;
};

/*
 * Configuration
 */

#define SHM_HASH_32(k)  sh_hash32((uint32_t)(k))
#define SHM_HASH_64(k)  sh_hash64((uint64_t)(k))
#define SHM_HASH_F(k)   sh_hash_f(k)
#define SHM_HASH_D(k)   sh_hash_d(k)

#ifdef S_FORCE_USING_MURMUR3
#define SHM_HASH_S ss_mh3_32
#else
#define SHM_HASH_S ss_fnv1a
#endif

/*
 * Allocation
 */

S_INLINE uint8_t shm_elem_size(int t)
{
    switch (t) {
%map map_types:single, map_types:pair_num, map_types:pair_ptr %{
        case SHM0_$U{name}: return sizeof(struct SHMap${stype});
%}
        default: break;
    }
    return 0;
}

S_INLINE size_t sh_hdr0_size(void)
{
    size_t as = sizeof(void *);
    return (sizeof(srt_hmap) / as) * as + ((sizeof(srt_hmap) % as) ? as : 0);
}

S_INLINE size_t sh_hdr_size(int t, uint64_t np2_elems)
{
    size_t h0s = sh_hdr0_size(), hs, es = shm_elem_size(t), hsr;
    uint64_t hs64 = h0s + np2_elems * sizeof(struct SHMBucket);
    hs = (size_t)hs64;
    RETURN_IF((uint64_t)hs != hs64, 0);
    hsr = es ? hs % es : 0;
    return hsr ? hs - hsr + es : hs;
}

%map {shm_get_buckets,shm_get_buckets_r}{,const}(fn,tmod) %{
S_INLINE ${tmod} struct SHMBucket *${fn}(${tmod} srt_hmap *hm) {
    return (${tmod} struct SHMBucket *)((${tmod} uint8_t *)hm
                                        + sh_hdr0_size());
}
%}

S_INLINE unsigned shm_s2hb(size_t max_size)
{
    unsigned hbits = slog2_ceil(max_size);
    return hbits ? hbits : 1;
}

/*
#API: |Allocate hash map (stack)|hash map type; initial reserve|hmap|O(n)|1;2|
srt_hmap *shm_alloca(enum eSHM_Type t, size_t n);
*/
#define shm_alloca(type, max_size)                             \
    shm_alloc_raw(type, S_TRUE,                                \
             s_alloca(sd_alloc_size_raw(                       \
                sh_hdr_size(type, snextpow2(max_size)),        \
                shm_elem_size(type), max_size, S_FALSE)),      \
             sh_hdr_size(type, (size_t)snextpow2(max_size)),   \
             shm_elem_size(type), max_size, shm_s2hb(max_size))

srt_hmap *shm_alloc_raw(int t, srt_bool ext_buf, void *buffer, size_t hdr_size,
            size_t elem_size, size_t max_size, size_t np2_size);

srt_hmap *shm_alloc_aux(int t, size_t init_size);

/* #api: |allocate hash map (heap)|hash map type; initial reserve|hmap|O(n)|1;2| */
S_INLINE srt_hmap *shm_alloc(enum eSHM_Type t, size_t init_size)
{
    return shm_alloc_aux((int)t, init_size);
}

%include "saux/sd_buildfuncs.hm"
%recall sd_buildfuncs:full_st (`shm`, `srt_hmap`, `0`)

/*
#API: |Ensure space for extra elements|hash map;number of extra elements|extra size allocated|O(1)|1;2|
size_t shm_grow(srt_hmap **hm, size_t extra_elems)

#API: |Ensure space for elements|hash map;absolute element reserve|reserved elements|O(1)|1;2|
size_t shm_reserve(srt_hmap **hm, size_t max_elems)

#API: |Make the hmap use the minimum possible memory|hmap|hmap reference (optional usage)|O(1) for allocators using memory remap; O(n) for naive allocators|1;2|
srt_hmap *shm_shrink(srt_hmap **hm);

#API: |Get hmap size|hmap|Hash map number of elements|O(1)|1;2|
size_t shm_size(const srt_hmap *hm);

#API: |Get hmap size|hmap|Hash map current max number of elements|O(1)|1;2|
size_t shm_max_size(const srt_hmap *hm);

#API: |Allocated space|hmap|current allocated space (vector elements)|O(1)|1;2|
size_t shm_capacity(const srt_hmap *hm);

#API: |Preallocated space left|hmap|allocated space left|O(1)|1;2|
size_t shm_capacity_left(const srt_hmap *hm);

#API: |Tells if a hash map is empty (zero elements)|hmap|S_TRUE: empty; S_FALSE: not empty|O(1)|1;2|
srt_bool shm_empty(const srt_hmap *hm)
*/

/* #API: |Duplicate hash map|input map|output map|O(n)|1;2| */
srt_hmap *shm_dup(const srt_hmap *src);

/* #API: |Clear/reset map (keeping map type)|hmap||O(1) for simple maps, O(n) for maps having nodes with strings|0;1| */
void shm_clear(srt_hmap *hm);

/*
#API: |Free one or more hash maps|hash map; more hash maps (optional)|-|O(1) for simple dmaps, O(n) for dmaps having nodes with strings|1;2|
void shm_free(srt_hmap **hm, ...)
*/
#ifdef S_USE_VA_ARGS
#define shm_free(...) shm_free_aux(__VA_ARGS__, S_INVALID_PTR_VARG_TAIL)
#else
#define shm_free(m) shm_free_aux(m, S_INVALID_PTR_VARG_TAIL)
#endif
void shm_free_aux(srt_hmap **s, ...);

/*
 * Copy
 */

/* #API: |Overwrite map with a map copy|output hash map; input map|output map reference (optional usage)|O(n)|0;1| */
srt_hmap *shm_cpy(srt_hmap **hm, const srt_hmap *src);

/*
 * Random access
 */

const void *shm_at(const srt_hmap *hm, uint32_t h, const void *key, uint32_t *tl);

S_INLINE const void *shm_at_s(const srt_hmap *hm, uint32_t h, const void *key, uint32_t *tl)
{
    return hm ? shm_at(hm, h, key, tl) : NULL;
}

%map map_types:pair_num, map_types:pair_ptr %{
/* #API: |Access to element (SHM_$U{name})|hash map; key|value|O(n), O(1) average amortized|1;2| */
S_INLINE %strsub [regex] ($b{vtype},`\*$$`,` *`,`\([^\*]\)$$`,`\1 `)shm_at_${name}(const srt_hmap *hm, ${ktype} k)
{
    const struct SHMap${stype} *e = (const struct SHMap${stype} *)
                shm_at_s(hm, %tabget shmap_kclass (#name,$b{kclass},#hash)(k),
                         %strcmp($b{sv_ktype},`gen`,``,`&`)k, NULL);
    return e ? %| ${class}:vget_shmap (`e`) |% : %| ${class}:vdef |%;
}

/* #API: |Access to element (SHM_$U{name})|hash map; key; value pointer|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE srt_bool shm_atp_${name}(const srt_hmap *hm, ${ktype} k, ${vtype} *v)
{
    const struct SHMap${stype} *e = (const struct SHMap${stype} *)
                shm_at_s(hm, %tabget shmap_kclass (#name,$b{kclass},#hash)(k),
                         %strcmp($b{sv_ktype},`gen`,``,`&`)k, NULL);
    if(e && v) {
        *v = %| ${class}:vget_shmap (`e`) |%;
        return S_TRUE;
    } else
        return S_FALSE;
}
%}

/*
 * Existence check
 */

%map map_types:single %{
/* #API: |Map element count/check (SHM_${doc_family})|hash map; key|S_TRUE: element found; S_FALSE: not in the map|O(n), O(1) average amortized|1;2| */
S_INLINE size_t shm_count_${name}(const srt_hmap *hm, ${ktype} k)
{
    return shm_at_s(hm, %tabget shmap_kclass (#name,$b{kclass},#hash)(k),
                    %strcmp($b{sv_ktype},`gen`,``,`&`)k, NULL) ? 1 : 0;
}
%}

/*
 * Insert
 */

%map map_types:pair_num, map_types:pair_ptr %{
/* #API: |Insert into map (SHM_$U{name})|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_insert_${name}(srt_hmap **hm, ${ktype} k, ${vtype} v);
%}

/* Hash set support (proxy) */

%map map_types:single %{
srt_bool shm_insert_${name}(srt_hmap **hm, ${ktype} k);
%}

/*
 * Increment
 */

%map map_types:pair_num %{
/* #API: |Increment value map element (SHM_$U{name})|hash map; key; value|S_TRUE: OK, S_FALSE: insertion error|O(n), O(1) average amortized|1;2| */
srt_bool shm_inc_${name}(srt_hmap **hm, ${ktype} k, ${vtype} v);
%}

/*
 * Delete
 */

%map map_types:single %{
/* #API: |Delete map element (SHM_${doc_family})|hash map; key|S_TRUE: found and deleted; S_FALSE: not found|O(n), O(1) average amortized|1;2| */
srt_bool shm_delete_${name}(srt_hmap *hm, ${ktype} k);
%}

/*
 * Enumeration
 */

S_INLINE const uint8_t *shm_enum_r(const srt_hmap *h, size_t i)
{
    return h && i < shm_size(h) ? shm_get_buffer_r(h) + i * h->d.elem_size : NULL;
}

%map map_types:single %{
/* #API: |Enumerate map keys (SHM_$U{name})|hash map; element, 0 to n - 1|${doc_ktype}|O(1)|1;2| */
S_INLINE %strsub [regex] ($b{ktype},`\*$$`,` *`,`\([^\*]\)$$`,`\1 `)shm_it_${name}_k(const srt_hmap *hm, size_t i)
{
    const struct SHMap${stype} *n = (const struct SHMap${stype} *)shm_enum_r(hm, i);
    RETURN_IF(!n, ${kdef});
    return %| ${class}:kget (`n`) |%;
}
%}

%map map_types:pair_num, map_types:pair_ptr %{
/* #API: |Enumerate map values (SHM_$U{name})|hash map; element, 0 to n - 1|${doc_vtype}|O(1)|1;2| */
S_INLINE %strsub [regex] ($b{vtype},`\*$$`,` *`,`\([^\*]\)$$`,`\1 `)shm_it_${name}_v(const srt_hmap *hm, size_t i)
{
    const struct SHMap${stype} *e;
    RETURN_IF(!hm || SHM_$U{name} != hm->d.sub_type, %| ${class}:vdef |%);
    e = (const struct SHMap${stype} *)shm_enum_r(hm, i);
    RETURN_IF(!e, %| ${class}:vdef |%);
    return %| ${class}:vget_shmap (`e`) |%;
}
%}

/*
 * Enumeration, with callback helper
 */

%map map_types:pair_num, map_types:pair_ptr %{
typedef srt_bool (*srt_hmap_it_${name})(${ktype} k, ${vtype} v, void *context);
%}

%map map_types:pair_num, map_types:pair_ptr %{
/* #API: |Enumerate map elements in portions (SHM_$U{name})|map; index start; index end; callback function; callback function context|Elements processed|O(n)|1;2| */
size_t shm_itp_${name}(const srt_hmap *m, size_t begin, size_t end, srt_hmap_it_${name} f, void *context);
%}

#ifdef __cplusplus
} /* extern "C" { */
#endif
#endif /* #ifndef SHMAP_H */

